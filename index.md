//[example_android](index.md)

# example_android

## Packages

| Name |
|---|
| [multi.platform.auth.example](example_android/multi.platform.auth.example/index.md) |
| [multi.platform.auth.example.app](example_android/multi.platform.auth.example.app/index.md) |
| [multi.platform.auth.example.app.profile](example_android/multi.platform.auth.example.app.profile/index.md) |
| [multi.platform.auth.example.data.profile](example_android/multi.platform.auth.example.data.profile/index.md) |
| [multi.platform.auth.example.data.profile.network](example_android/multi.platform.auth.example.data.profile.network/index.md) |
| [multi.platform.auth.example.domain.profile](example_android/multi.platform.auth.example.domain.profile/index.md) |
| [multi.platform.auth.example.domain.profile.entity](example_android/multi.platform.auth.example.domain.profile.entity/index.md) |
| [multi.platform.auth.example.domain.profile.usecase](example_android/multi.platform.auth.example.domain.profile.usecase/index.md) |
| [multi.platform.auth.example.external](example_android/multi.platform.auth.example.external/index.md) |

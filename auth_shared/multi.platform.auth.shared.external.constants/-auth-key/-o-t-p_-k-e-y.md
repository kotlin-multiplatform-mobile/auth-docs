//[auth_shared](../../../index.md)/[multi.platform.auth.shared.external.constants](../index.md)/[AuthKey](index.md)/[OTP_KEY](-o-t-p_-k-e-y.md)

# OTP_KEY

[common]\
const val [OTP_KEY](-o-t-p_-k-e-y.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)

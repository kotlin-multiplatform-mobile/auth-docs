//[auth_shared](../../../index.md)/[multi.platform.auth.shared.external.constants](../index.md)/[AuthKey](index.md)/[REGISTER_KEY](-r-e-g-i-s-t-e-r_-k-e-y.md)

# REGISTER_KEY

[common]\
const val [REGISTER_KEY](-r-e-g-i-s-t-e-r_-k-e-y.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)

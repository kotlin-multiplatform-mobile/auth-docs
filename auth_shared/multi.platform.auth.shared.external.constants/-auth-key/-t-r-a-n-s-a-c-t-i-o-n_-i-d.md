//[auth_shared](../../../index.md)/[multi.platform.auth.shared.external.constants](../index.md)/[AuthKey](index.md)/[TRANSACTION_ID](-t-r-a-n-s-a-c-t-i-o-n_-i-d.md)

# TRANSACTION_ID

[common]\
const val [TRANSACTION_ID](-t-r-a-n-s-a-c-t-i-o-n_-i-d.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)

//[auth_shared](../../../index.md)/[multi.platform.auth.shared.external.constants](../index.md)/[AuthKey](index.md)

# AuthKey

[common]\
object [AuthKey](index.md)

## Properties

| Name | Summary |
|---|---|
| [COUNTRY_KEY](-c-o-u-n-t-r-y_-k-e-y.md) | [common]<br>const val [COUNTRY_KEY](-c-o-u-n-t-r-y_-k-e-y.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) |
| [FORGET_PASSWORD_KEY](-f-o-r-g-e-t_-p-a-s-s-w-o-r-d_-k-e-y.md) | [common]<br>const val [FORGET_PASSWORD_KEY](-f-o-r-g-e-t_-p-a-s-s-w-o-r-d_-k-e-y.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) |
| [OTP_KEY](-o-t-p_-k-e-y.md) | [common]<br>const val [OTP_KEY](-o-t-p_-k-e-y.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) |
| [REGISTER_KEY](-r-e-g-i-s-t-e-r_-k-e-y.md) | [common]<br>const val [REGISTER_KEY](-r-e-g-i-s-t-e-r_-k-e-y.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) |
| [SIGN_IN_KEY](-s-i-g-n_-i-n_-k-e-y.md) | [common]<br>const val [SIGN_IN_KEY](-s-i-g-n_-i-n_-k-e-y.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) |
| [SIGN_OUT_KEY](-s-i-g-n_-o-u-t_-k-e-y.md) | [common]<br>const val [SIGN_OUT_KEY](-s-i-g-n_-o-u-t_-k-e-y.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) |
| [TRANSACTION_ID](-t-r-a-n-s-a-c-t-i-o-n_-i-d.md) | [common]<br>const val [TRANSACTION_ID](-t-r-a-n-s-a-c-t-i-o-n_-i-d.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) |
| [VERIFY_KEY](-v-e-r-i-f-y_-k-e-y.md) | [common]<br>const val [VERIFY_KEY](-v-e-r-i-f-y_-k-e-y.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) |

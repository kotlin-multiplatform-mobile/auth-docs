//[auth_shared](../../../index.md)/[multi.platform.auth.shared.external.constants](../index.md)/[AuthKey](index.md)/[VERIFY_KEY](-v-e-r-i-f-y_-k-e-y.md)

# VERIFY_KEY

[common]\
const val [VERIFY_KEY](-v-e-r-i-f-y_-k-e-y.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)

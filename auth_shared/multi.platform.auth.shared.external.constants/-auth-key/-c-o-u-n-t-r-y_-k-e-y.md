//[auth_shared](../../../index.md)/[multi.platform.auth.shared.external.constants](../index.md)/[AuthKey](index.md)/[COUNTRY_KEY](-c-o-u-n-t-r-y_-k-e-y.md)

# COUNTRY_KEY

[common]\
const val [COUNTRY_KEY](-c-o-u-n-t-r-y_-k-e-y.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)

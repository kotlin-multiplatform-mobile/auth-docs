//[auth_shared](../../../index.md)/[multi.platform.auth.shared.external.constants](../index.md)/[AuthKey](index.md)/[FORGET_PASSWORD_KEY](-f-o-r-g-e-t_-p-a-s-s-w-o-r-d_-k-e-y.md)

# FORGET_PASSWORD_KEY

[common]\
const val [FORGET_PASSWORD_KEY](-f-o-r-g-e-t_-p-a-s-s-w-o-r-d_-k-e-y.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)

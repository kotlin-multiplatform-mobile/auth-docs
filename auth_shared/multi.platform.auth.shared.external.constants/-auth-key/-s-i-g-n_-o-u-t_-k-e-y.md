//[auth_shared](../../../index.md)/[multi.platform.auth.shared.external.constants](../index.md)/[AuthKey](index.md)/[SIGN_OUT_KEY](-s-i-g-n_-o-u-t_-k-e-y.md)

# SIGN_OUT_KEY

[common]\
const val [SIGN_OUT_KEY](-s-i-g-n_-o-u-t_-k-e-y.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)

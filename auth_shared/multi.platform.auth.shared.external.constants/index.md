//[auth_shared](../../index.md)/[multi.platform.auth.shared.external.constants](index.md)

# Package-level declarations

## Types

| Name | Summary |
|---|---|
| [AuthKey](-auth-key/index.md) | [common]<br>object [AuthKey](-auth-key/index.md) |

//[auth_shared](../../../index.md)/[multi.platform.auth.shared.domain.auth.entity](../index.md)/[Otp](index.md)/[duration](duration.md)

# duration

[common]\
var [duration](duration.md): [Long](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-long/index.html)?

//[auth_shared](../../../index.md)/[multi.platform.auth.shared.domain.auth.entity](../index.md)/[Session](index.md)/[fullname](fullname.md)

# fullname

[common]\
var [fullname](fullname.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?

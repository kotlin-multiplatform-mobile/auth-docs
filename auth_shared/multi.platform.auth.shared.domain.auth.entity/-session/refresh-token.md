//[auth_shared](../../../index.md)/[multi.platform.auth.shared.domain.auth.entity](../index.md)/[Session](index.md)/[refreshToken](refresh-token.md)

# refreshToken

[common]\
var [refreshToken](refresh-token.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?

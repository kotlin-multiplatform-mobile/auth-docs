//[auth_shared](../../../index.md)/[multi.platform.auth.shared.domain.auth.entity](../index.md)/[Session](index.md)/[isEmailVerified](is-email-verified.md)

# isEmailVerified

[common]\
var [isEmailVerified](is-email-verified.md): [Boolean](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html)?

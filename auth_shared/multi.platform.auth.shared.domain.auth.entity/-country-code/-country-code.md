//[auth_shared](../../../index.md)/[multi.platform.auth.shared.domain.auth.entity](../index.md)/[CountryCode](index.md)/[CountryCode](-country-code.md)

# CountryCode

[common]\
constructor(code: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html), dialCode: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html), countryName: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html))

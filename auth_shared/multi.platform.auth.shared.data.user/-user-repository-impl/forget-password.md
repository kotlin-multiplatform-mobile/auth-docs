//[auth_shared](../../../index.md)/[multi.platform.auth.shared.data.user](../index.md)/[UserRepositoryImpl](index.md)/[forgetPassword](forget-password.md)

# forgetPassword

[common]\
open suspend override fun [forgetPassword](forget-password.md)(email: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)): JsonObject?

//[auth_shared](../../../index.md)/[multi.platform.auth.shared.data.user](../index.md)/[UserRepositoryImpl](index.md)/[verifyOtp](verify-otp.md)

# verifyOtp

[common]\
open suspend override fun [verifyOtp](verify-otp.md)(otp: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html), type: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html), phone: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)): JsonObject?

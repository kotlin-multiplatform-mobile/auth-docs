//[auth_shared](../../../index.md)/[multi.platform.auth.shared.data.user](../index.md)/[UserRepositoryImpl](index.md)/[signInEmail](sign-in-email.md)

# signInEmail

[common]\
open suspend override fun [signInEmail](sign-in-email.md)(email: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html), password: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)): JsonObject?

//[auth_shared](../../../index.md)/[multi.platform.auth.shared.data.user](../index.md)/[AuthRepositoryImpl](index.md)/[authorization](authorization.md)

# authorization

[common]\
open suspend override fun [authorization](authorization.md)(phone: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)): JsonObject?

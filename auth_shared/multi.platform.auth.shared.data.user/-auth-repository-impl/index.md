//[auth_shared](../../../index.md)/[multi.platform.auth.shared.data.user](../index.md)/[AuthRepositoryImpl](index.md)

# AuthRepositoryImpl

[common]\
class [AuthRepositoryImpl](index.md)(authConfig: [AuthConfig](../../multi.platform.auth.shared.external/-auth-config/index.md), apiClientProvider: ApiClientProvider&lt;HttpClient&gt;) : [AuthRepository](../../multi.platform.auth.shared.domain.auth/-auth-repository/index.md)

## Constructors

| | |
|---|---|
| [AuthRepositoryImpl](-auth-repository-impl.md) | [common]<br>constructor(authConfig: [AuthConfig](../../multi.platform.auth.shared.external/-auth-config/index.md), apiClientProvider: ApiClientProvider&lt;HttpClient&gt;) |

## Functions

| Name | Summary |
|---|---|
| [authorization](authorization.md) | [common]<br>open suspend override fun [authorization](authorization.md)(phone: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)): JsonObject? |
| [forgetPassword](forget-password.md) | [common]<br>open suspend override fun [forgetPassword](forget-password.md)(email: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)): JsonObject? |
| [register](register.md) | [common]<br>open suspend override fun [register](register.md)(trxid: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html), userReq: [UserReq](../../multi.platform.auth.shared.data.user.network.request/-user-req/index.md), imageBytes: [ByteArray](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-byte-array/index.html)?, imageName: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?): JsonObject? |
| [signInEmail](sign-in-email.md) | [common]<br>open suspend override fun [signInEmail](sign-in-email.md)(email: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html), password: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)): JsonObject? |
| [signInProvider](sign-in-provider.md) | [common]<br>open suspend override fun [signInProvider](sign-in-provider.md)(authType: [AuthType](../../multi.platform.auth.shared.external.enums/-auth-type/index.md), token: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html), userReq: [UserReq](../../multi.platform.auth.shared.data.user.network.request/-user-req/index.md)?): JsonObject? |
| [signOut](sign-out.md) | [common]<br>open suspend override fun [signOut](sign-out.md)(accessToken: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?): JsonObject? |
| [validatePhone](validate-phone.md) | [common]<br>open suspend override fun [validatePhone](validate-phone.md)(phone: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)): JsonObject? |
| [verifyOtp](verify-otp.md) | [common]<br>open suspend override fun [verifyOtp](verify-otp.md)(otp: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html), type: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html), phone: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)): JsonObject? |

//[auth_shared](../../../index.md)/[multi.platform.auth.shared.data.user](../index.md)/[AuthRepositoryImpl](index.md)/[signInProvider](sign-in-provider.md)

# signInProvider

[common]\
open suspend override fun [signInProvider](sign-in-provider.md)(authType: [AuthType](../../multi.platform.auth.shared.external.enums/-auth-type/index.md), token: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html), userReq: [UserReq](../../multi.platform.auth.shared.data.user.network.request/-user-req/index.md)?): JsonObject?

//[auth_shared](../../../index.md)/[multi.platform.auth.shared.external](../index.md)/[AuthConfig](index.md)/[headerTransactionIdKey](header-transaction-id-key.md)

# headerTransactionIdKey

[common]\
abstract val [headerTransactionIdKey](header-transaction-id-key.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)

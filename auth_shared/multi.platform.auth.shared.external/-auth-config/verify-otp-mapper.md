//[auth_shared](../../../index.md)/[multi.platform.auth.shared.external](../index.md)/[AuthConfig](index.md)/[verifyOtpMapper](verify-otp-mapper.md)

# verifyOtpMapper

[common]\
abstract fun [verifyOtpMapper](verify-otp-mapper.md)(jsonObject: JsonObject?): [Ticket](../../multi.platform.auth.shared.domain.auth.entity/-ticket/index.md)?

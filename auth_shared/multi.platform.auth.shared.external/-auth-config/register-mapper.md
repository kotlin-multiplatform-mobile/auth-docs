//[auth_shared](../../../index.md)/[multi.platform.auth.shared.external](../index.md)/[AuthConfig](index.md)/[registerMapper](register-mapper.md)

# registerMapper

[common]\
abstract fun [registerMapper](register-mapper.md)(jsonObject: JsonObject?): [Ticket](../../multi.platform.auth.shared.domain.auth.entity/-ticket/index.md)?

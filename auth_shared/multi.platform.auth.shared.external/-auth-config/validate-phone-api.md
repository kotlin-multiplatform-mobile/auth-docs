//[auth_shared](../../../index.md)/[multi.platform.auth.shared.external](../index.md)/[AuthConfig](index.md)/[validatePhoneApi](validate-phone-api.md)

# validatePhoneApi

[common]\
abstract val [validatePhoneApi](validate-phone-api.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)

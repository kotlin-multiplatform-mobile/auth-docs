//[auth_shared](../../../index.md)/[multi.platform.auth.shared.external](../index.md)/[AuthConfig](index.md)/[signOutApi](sign-out-api.md)

# signOutApi

[common]\
abstract val [signOutApi](sign-out-api.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)

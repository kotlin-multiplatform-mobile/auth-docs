//[auth_shared](../../../index.md)/[multi.platform.auth.shared.external](../index.md)/[AuthConfig](index.md)/[signOutMapper](sign-out-mapper.md)

# signOutMapper

[common]\
abstract fun [signOutMapper](sign-out-mapper.md)(jsonObject: JsonObject?): [Ticket](../../multi.platform.auth.shared.domain.auth.entity/-ticket/index.md)?

//[auth_shared](../../../index.md)/[multi.platform.auth.shared.external](../index.md)/[AuthConfig](index.md)/[signInByProviderApi](sign-in-by-provider-api.md)

# signInByProviderApi

[common]\
abstract val [signInByProviderApi](sign-in-by-provider-api.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)

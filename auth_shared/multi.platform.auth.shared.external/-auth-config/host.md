//[auth_shared](../../../index.md)/[multi.platform.auth.shared.external](../index.md)/[AuthConfig](index.md)/[host](host.md)

# host

[common]\
abstract val [host](host.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)

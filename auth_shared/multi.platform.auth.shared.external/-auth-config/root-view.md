//[auth_shared](../../../index.md)/[multi.platform.auth.shared.external](../index.md)/[AuthConfig](index.md)/[rootView](root-view.md)

# rootView

[common]\
abstract val [rootView](root-view.md): [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html)

//[auth_shared](../../../index.md)/[multi.platform.auth.shared.external](../index.md)/[AuthConfig](index.md)/[signInByEmailApi](sign-in-by-email-api.md)

# signInByEmailApi

[common]\
abstract val [signInByEmailApi](sign-in-by-email-api.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)

//[auth_shared](../../../index.md)/[multi.platform.auth.shared.external](../index.md)/[AuthConfig](index.md)/[signInByPhoneApi](sign-in-by-phone-api.md)

# signInByPhoneApi

[common]\
abstract val [signInByPhoneApi](sign-in-by-phone-api.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)

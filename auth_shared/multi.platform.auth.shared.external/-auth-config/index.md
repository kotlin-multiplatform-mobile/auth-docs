//[auth_shared](../../../index.md)/[multi.platform.auth.shared.external](../index.md)/[AuthConfig](index.md)

# AuthConfig

[common]\
interface [AuthConfig](index.md)

## Properties

| Name | Summary |
|---|---|
| [countryFlag](country-flag.md) | [common]<br>abstract val [countryFlag](country-flag.md): [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html) |
| [forgetPasswordApi](forget-password-api.md) | [common]<br>abstract val [forgetPasswordApi](forget-password-api.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) |
| [headerTransactionIdKey](header-transaction-id-key.md) | [common]<br>abstract val [headerTransactionIdKey](header-transaction-id-key.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) |
| [host](host.md) | [common]<br>abstract val [host](host.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) |
| [logo](logo.md) | [common]<br>abstract val [logo](logo.md): [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html) |
| [registerApi](register-api.md) | [common]<br>abstract val [registerApi](register-api.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) |
| [rootView](root-view.md) | [common]<br>abstract val [rootView](root-view.md): [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html) |
| [signInByEmailApi](sign-in-by-email-api.md) | [common]<br>abstract val [signInByEmailApi](sign-in-by-email-api.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) |
| [signInByPhoneApi](sign-in-by-phone-api.md) | [common]<br>abstract val [signInByPhoneApi](sign-in-by-phone-api.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) |
| [signInByProviderApi](sign-in-by-provider-api.md) | [common]<br>abstract val [signInByProviderApi](sign-in-by-provider-api.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) |
| [signOutApi](sign-out-api.md) | [common]<br>abstract val [signOutApi](sign-out-api.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) |
| [validatePhoneApi](validate-phone-api.md) | [common]<br>abstract val [validatePhoneApi](validate-phone-api.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) |
| [verifyOtpApi](verify-otp-api.md) | [common]<br>abstract val [verifyOtpApi](verify-otp-api.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) |

## Functions

| Name | Summary |
|---|---|
| [forgetPasswordMapper](forget-password-mapper.md) | [common]<br>abstract fun [forgetPasswordMapper](forget-password-mapper.md)(jsonObject: JsonObject?): [Ticket](../../multi.platform.auth.shared.domain.auth.entity/-ticket/index.md)? |
| [registerMapper](register-mapper.md) | [common]<br>abstract fun [registerMapper](register-mapper.md)(jsonObject: JsonObject?): [Ticket](../../multi.platform.auth.shared.domain.auth.entity/-ticket/index.md)? |
| [signInMapper](sign-in-mapper.md) | [common]<br>abstract fun [signInMapper](sign-in-mapper.md)(jsonObject: JsonObject?): [Ticket](../../multi.platform.auth.shared.domain.auth.entity/-ticket/index.md)? |
| [signOutMapper](sign-out-mapper.md) | [common]<br>abstract fun [signOutMapper](sign-out-mapper.md)(jsonObject: JsonObject?): [Ticket](../../multi.platform.auth.shared.domain.auth.entity/-ticket/index.md)? |
| [validatePhoneMapper](validate-phone-mapper.md) | [common]<br>abstract fun [validatePhoneMapper](validate-phone-mapper.md)(jsonObject: JsonObject?): [Ticket](../../multi.platform.auth.shared.domain.auth.entity/-ticket/index.md)? |
| [verifyOtpMapper](verify-otp-mapper.md) | [common]<br>abstract fun [verifyOtpMapper](verify-otp-mapper.md)(jsonObject: JsonObject?): [Ticket](../../multi.platform.auth.shared.domain.auth.entity/-ticket/index.md)? |

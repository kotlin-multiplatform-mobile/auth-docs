//[auth_shared](../../../index.md)/[multi.platform.auth.shared.external](../index.md)/[AuthConfig](index.md)/[registerApi](register-api.md)

# registerApi

[common]\
abstract val [registerApi](register-api.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)

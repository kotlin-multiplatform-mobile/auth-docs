//[auth_shared](../../../index.md)/[multi.platform.auth.shared.external](../index.md)/[AuthConfig](index.md)/[signInMapper](sign-in-mapper.md)

# signInMapper

[common]\
abstract fun [signInMapper](sign-in-mapper.md)(jsonObject: JsonObject?): [Ticket](../../multi.platform.auth.shared.domain.auth.entity/-ticket/index.md)?

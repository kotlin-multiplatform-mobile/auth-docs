//[auth_shared](../../../index.md)/[multi.platform.auth.shared.external](../index.md)/[AuthConfig](index.md)/[logo](logo.md)

# logo

[common]\
abstract val [logo](logo.md): [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html)

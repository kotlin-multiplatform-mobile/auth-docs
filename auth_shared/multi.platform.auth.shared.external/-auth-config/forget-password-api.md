//[auth_shared](../../../index.md)/[multi.platform.auth.shared.external](../index.md)/[AuthConfig](index.md)/[forgetPasswordApi](forget-password-api.md)

# forgetPasswordApi

[common]\
abstract val [forgetPasswordApi](forget-password-api.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)

//[auth_shared](../../../index.md)/[multi.platform.auth.shared.external](../index.md)/[AuthConfig](index.md)/[verifyOtpApi](verify-otp-api.md)

# verifyOtpApi

[common]\
abstract val [verifyOtpApi](verify-otp-api.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)

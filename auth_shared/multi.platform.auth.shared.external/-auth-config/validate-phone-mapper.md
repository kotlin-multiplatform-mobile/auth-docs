//[auth_shared](../../../index.md)/[multi.platform.auth.shared.external](../index.md)/[AuthConfig](index.md)/[validatePhoneMapper](validate-phone-mapper.md)

# validatePhoneMapper

[common]\
abstract fun [validatePhoneMapper](validate-phone-mapper.md)(jsonObject: JsonObject?): [Ticket](../../multi.platform.auth.shared.domain.auth.entity/-ticket/index.md)?

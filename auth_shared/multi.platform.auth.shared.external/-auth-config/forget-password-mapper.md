//[auth_shared](../../../index.md)/[multi.platform.auth.shared.external](../index.md)/[AuthConfig](index.md)/[forgetPasswordMapper](forget-password-mapper.md)

# forgetPasswordMapper

[common]\
abstract fun [forgetPasswordMapper](forget-password-mapper.md)(jsonObject: JsonObject?): [Ticket](../../multi.platform.auth.shared.domain.auth.entity/-ticket/index.md)?

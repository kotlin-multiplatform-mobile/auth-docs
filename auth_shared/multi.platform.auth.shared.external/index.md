//[auth_shared](../../index.md)/[multi.platform.auth.shared.external](index.md)

# Package-level declarations

## Types

| Name | Summary |
|---|---|
| [AuthConfig](-auth-config/index.md) | [common]<br>interface [AuthConfig](-auth-config/index.md) |

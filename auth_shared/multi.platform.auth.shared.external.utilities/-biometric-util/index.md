//[auth_shared](../../../index.md)/[multi.platform.auth.shared.external.utilities](../index.md)/[BiometricUtil](index.md)

# BiometricUtil

[android]\
class [BiometricUtil](index.md)(context: [Context](https://developer.android.com/reference/kotlin/android/content/Context.html), androidKeyStore: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html), onAuthenticationSucceeded: ([String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)) -&gt; [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html))

## Constructors

| | |
|---|---|
| [BiometricUtil](-biometric-util.md) | [android]<br>constructor(context: [Context](https://developer.android.com/reference/kotlin/android/content/Context.html), androidKeyStore: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html), onAuthenticationSucceeded: ([String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)) -&gt; [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)) |

## Functions

| Name | Summary |
|---|---|
| [show](show.md) | [android]<br>fun [show](show.md)() |

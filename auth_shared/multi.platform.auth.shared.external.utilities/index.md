//[auth_shared](../../index.md)/[multi.platform.auth.shared.external.utilities](index.md)

# Package-level declarations

## Types

| Name | Summary |
|---|---|
| [BiometricUtil](-biometric-util/index.md) | [android]<br>class [BiometricUtil](-biometric-util/index.md)(context: [Context](https://developer.android.com/reference/kotlin/android/content/Context.html), androidKeyStore: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html), onAuthenticationSucceeded: ([String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)) -&gt; [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)) |

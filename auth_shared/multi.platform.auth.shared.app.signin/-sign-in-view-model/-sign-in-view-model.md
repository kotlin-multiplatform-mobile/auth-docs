//[auth_shared](../../../index.md)/[multi.platform.auth.shared.app.signin](../index.md)/[SignInViewModel](index.md)/[SignInViewModel](-sign-in-view-model.md)

# SignInViewModel

[common]\
constructor(authorizationUseCase: [AuthorizationUseCase](../../multi.platform.auth.shared.domain.auth.usecase/-authorization-use-case/index.md), validatePhoneUseCase: [ValidatePhoneUseCase](../../multi.platform.auth.shared.domain.auth.usecase/-validate-phone-use-case/index.md), signInEmailUseCase: [SignInEmailUseCase](../../multi.platform.auth.shared.domain.auth.usecase/-sign-in-email-use-case/index.md), signInProviderUseCase: [SignInProviderUseCase](../../multi.platform.auth.shared.domain.auth.usecase/-sign-in-provider-use-case/index.md))

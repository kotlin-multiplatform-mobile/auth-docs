//[auth_shared](../../../index.md)/[multi.platform.auth.shared.app.signin](../index.md)/[SignInViewModel](index.md)/[onSignInByBiometricClick](on-sign-in-by-biometric-click.md)

# onSignInByBiometricClick

[common]\
val [onSignInByBiometricClick](on-sign-in-by-biometric-click.md): MutableStateFlow&lt;[Boolean](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html)?&gt;

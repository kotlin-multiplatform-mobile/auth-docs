//[auth_shared](../../../index.md)/[multi.platform.auth.shared.app.signin](../index.md)/[SignInViewModel](index.md)/[onSignInByGoogleClick](on-sign-in-by-google-click.md)

# onSignInByGoogleClick

[common]\
val [onSignInByGoogleClick](on-sign-in-by-google-click.md): MutableStateFlow&lt;[Boolean](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html)?&gt;

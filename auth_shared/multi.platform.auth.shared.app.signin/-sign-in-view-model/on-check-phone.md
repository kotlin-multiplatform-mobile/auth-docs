//[auth_shared](../../../index.md)/[multi.platform.auth.shared.app.signin](../index.md)/[SignInViewModel](index.md)/[onCheckPhone](on-check-phone.md)

# onCheckPhone

[common]\
val [onCheckPhone](on-check-phone.md): MutableStateFlow&lt;[Ticket](../../multi.platform.auth.shared.domain.auth.entity/-ticket/index.md)?&gt;

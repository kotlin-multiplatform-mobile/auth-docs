//[auth_shared](../../../index.md)/[multi.platform.auth.shared.app.signin](../index.md)/[SignInViewModel](index.md)/[password](password.md)

# password

[common]\
val [password](password.md): MutableStateFlow&lt;[String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?&gt;

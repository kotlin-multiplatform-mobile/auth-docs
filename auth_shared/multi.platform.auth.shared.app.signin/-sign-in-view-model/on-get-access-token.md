//[auth_shared](../../../index.md)/[multi.platform.auth.shared.app.signin](../index.md)/[SignInViewModel](index.md)/[onGetAccessToken](on-get-access-token.md)

# onGetAccessToken

[common]\
fun [onGetAccessToken](on-get-access-token.md)(authType: [AuthType](../../multi.platform.auth.shared.external.enums/-auth-type/index.md), accessToken: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html), userReq: [UserReq](../../multi.platform.auth.shared.data.auth.network.request/-user-req/index.md)?)

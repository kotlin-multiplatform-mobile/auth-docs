//[auth_shared](../../../index.md)/[multi.platform.auth.shared.app.signin](../index.md)/[SignInViewModel](index.md)/[onGoToRegisterClick](on-go-to-register-click.md)

# onGoToRegisterClick

[common]\
val [onGoToRegisterClick](on-go-to-register-click.md): MutableStateFlow&lt;[Boolean](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html)?&gt;

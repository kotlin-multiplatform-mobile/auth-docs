//[auth_shared](../../../index.md)/[multi.platform.auth.shared.app.signin](../index.md)/[SignInViewModel](index.md)/[onSignedIn](on-signed-in.md)

# onSignedIn

[common]\
val [onSignedIn](on-signed-in.md): MutableStateFlow&lt;[Ticket](../../multi.platform.auth.shared.domain.auth.entity/-ticket/index.md)?&gt;

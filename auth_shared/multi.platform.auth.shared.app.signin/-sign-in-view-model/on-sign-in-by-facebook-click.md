//[auth_shared](../../../index.md)/[multi.platform.auth.shared.app.signin](../index.md)/[SignInViewModel](index.md)/[onSignInByFacebookClick](on-sign-in-by-facebook-click.md)

# onSignInByFacebookClick

[common]\
val [onSignInByFacebookClick](on-sign-in-by-facebook-click.md): MutableStateFlow&lt;[Boolean](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html)?&gt;

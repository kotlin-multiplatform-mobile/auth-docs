//[auth_shared](../../../index.md)/[multi.platform.auth.shared.app.signin](../index.md)/[SignInViewModel](index.md)/[onGetAccessTokenFail](on-get-access-token-fail.md)

# onGetAccessTokenFail

[common]\
fun [onGetAccessTokenFail](on-get-access-token-fail.md)(error: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?)

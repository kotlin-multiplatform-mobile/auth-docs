//[auth_shared](../../../index.md)/[multi.platform.auth.shared.app.signin](../index.md)/[SignInViewModel](index.md)/[authType](auth-type.md)

# authType

[common]\
val [authType](auth-type.md): MutableStateFlow&lt;[AuthType](../../multi.platform.auth.shared.external.enums/-auth-type/index.md)?&gt;

//[auth_shared](../../../index.md)/[multi.platform.auth.shared.app.signin](../index.md)/[SignInViewModel](index.md)/[onGoToForgetPasswordClick](on-go-to-forget-password-click.md)

# onGoToForgetPasswordClick

[common]\
val [onGoToForgetPasswordClick](on-go-to-forget-password-click.md): MutableStateFlow&lt;[Boolean](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html)?&gt;

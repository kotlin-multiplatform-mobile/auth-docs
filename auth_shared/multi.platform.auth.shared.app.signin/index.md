//[auth_shared](../../index.md)/[multi.platform.auth.shared.app.signin](index.md)

# Package-level declarations

## Types

| Name | Summary |
|---|---|
| [SignInFragment](-sign-in-fragment/index.md) | [android]<br>class [SignInFragment](-sign-in-fragment/index.md) : CoreFragment |
| [SignInViewModel](-sign-in-view-model/index.md) | [common]<br>class [SignInViewModel](-sign-in-view-model/index.md)(authorizationUseCase: [AuthorizationUseCase](../multi.platform.auth.shared.domain.auth.usecase/-authorization-use-case/index.md), validatePhoneUseCase: [ValidatePhoneUseCase](../multi.platform.auth.shared.domain.auth.usecase/-validate-phone-use-case/index.md), signInEmailUseCase: [SignInEmailUseCase](../multi.platform.auth.shared.domain.auth.usecase/-sign-in-email-use-case/index.md), signInProviderUseCase: [SignInProviderUseCase](../multi.platform.auth.shared.domain.auth.usecase/-sign-in-provider-use-case/index.md)) : CoreViewModel |

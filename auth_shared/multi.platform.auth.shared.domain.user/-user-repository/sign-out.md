//[auth_shared](../../../index.md)/[multi.platform.auth.shared.domain.user](../index.md)/[UserRepository](index.md)/[signOut](sign-out.md)

# signOut

[common]\
abstract suspend fun [signOut](sign-out.md)(accessToken: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?): JsonObject?

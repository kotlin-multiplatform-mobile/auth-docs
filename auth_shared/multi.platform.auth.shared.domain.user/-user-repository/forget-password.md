//[auth_shared](../../../index.md)/[multi.platform.auth.shared.domain.user](../index.md)/[UserRepository](index.md)/[forgetPassword](forget-password.md)

# forgetPassword

[common]\
abstract suspend fun [forgetPassword](forget-password.md)(email: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)): JsonObject?

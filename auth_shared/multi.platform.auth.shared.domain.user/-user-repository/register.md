//[auth_shared](../../../index.md)/[multi.platform.auth.shared.domain.user](../index.md)/[UserRepository](index.md)/[register](register.md)

# register

[common]\
abstract suspend fun [register](register.md)(trxid: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html), userReq: [UserReq](../../multi.platform.auth.shared.data.user.network.request/-user-req/index.md), imageBytes: [ByteArray](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-byte-array/index.html)?, imageName: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?): JsonObject?

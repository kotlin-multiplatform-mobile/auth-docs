//[auth_shared](../../index.md)/[multi.platform.auth.shared.domain.user](index.md)

# Package-level declarations

## Types

| Name | Summary |
|---|---|
| [UserRepository](-user-repository/index.md) | [common]<br>interface [UserRepository](-user-repository/index.md) |

//[auth_shared](../../../index.md)/[multi.platform.auth.shared.data.user.network.request](../index.md)/[VerifyOtpReq](index.md)/[mode](mode.md)

# mode

[common]\
val [mode](mode.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? = null

//[auth_shared](../../../index.md)/[multi.platform.auth.shared.data.user.network.request](../index.md)/[VerifyOtpReq](index.md)/[otp](otp.md)

# otp

[common]\
val [otp](otp.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? = null

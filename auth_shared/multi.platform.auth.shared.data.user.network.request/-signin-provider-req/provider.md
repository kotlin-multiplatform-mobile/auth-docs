//[auth_shared](../../../index.md)/[multi.platform.auth.shared.data.user.network.request](../index.md)/[SigninProviderReq](index.md)/[provider](provider.md)

# provider

[common]\
val [provider](provider.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? = null

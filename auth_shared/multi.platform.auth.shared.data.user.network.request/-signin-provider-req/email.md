//[auth_shared](../../../index.md)/[multi.platform.auth.shared.data.user.network.request](../index.md)/[SigninProviderReq](index.md)/[email](email.md)

# email

[common]\
val [email](email.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? = null

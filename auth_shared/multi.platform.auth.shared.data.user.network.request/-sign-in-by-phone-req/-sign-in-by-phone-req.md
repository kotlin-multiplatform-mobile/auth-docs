//[auth_shared](../../../index.md)/[multi.platform.auth.shared.data.user.network.request](../index.md)/[SignInByPhoneReq](index.md)/[SignInByPhoneReq](-sign-in-by-phone-req.md)

# SignInByPhoneReq

[common]\
constructor(msisdn: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? = null)

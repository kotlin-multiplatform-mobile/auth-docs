//[auth_shared](../../../index.md)/[multi.platform.auth.shared.data.user.network.request](../index.md)/[UserReq](index.md)/[country](country.md)

# country

[common]\
val [country](country.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? = null

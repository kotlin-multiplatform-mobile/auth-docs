//[auth_shared](../../../index.md)/[multi.platform.auth.shared.data.user.network.request](../index.md)/[UserReq](index.md)/[id](id.md)

# id

[common]\
val [id](id.md): [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html) = 0

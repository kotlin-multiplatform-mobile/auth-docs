//[auth_shared](../../../index.md)/[multi.platform.auth.shared.domain.user.entity](../index.md)/[Otp](index.md)/[msisdn](msisdn.md)

# msisdn

[common]\
var [msisdn](msisdn.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?

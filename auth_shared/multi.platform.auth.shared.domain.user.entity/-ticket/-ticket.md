//[auth_shared](../../../index.md)/[multi.platform.auth.shared.domain.user.entity](../index.md)/[Ticket](index.md)/[Ticket](-ticket.md)

# Ticket

[common]\
constructor(timestamp: [Long](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-long/index.html)? = null, state: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? = null, otp: [Otp](../-otp/index.md)? = null, session: [Session](../-session/index.md)? = null, transactionId: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? = null)

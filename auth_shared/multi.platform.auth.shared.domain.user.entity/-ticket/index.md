//[auth_shared](../../../index.md)/[multi.platform.auth.shared.domain.user.entity](../index.md)/[Ticket](index.md)

# Ticket

[common]\
@Serializable

data class [Ticket](index.md)(var timestamp: [Long](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-long/index.html)? = null, var state: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? = null, var otp: [Otp](../-otp/index.md)? = null, var session: [Session](../-session/index.md)? = null, var transactionId: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? = null)

## Constructors

| | |
|---|---|
| [Ticket](-ticket.md) | [common]<br>constructor(timestamp: [Long](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-long/index.html)? = null, state: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? = null, otp: [Otp](../-otp/index.md)? = null, session: [Session](../-session/index.md)? = null, transactionId: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? = null) |

## Properties

| Name | Summary |
|---|---|
| [otp](otp.md) | [common]<br>var [otp](otp.md): [Otp](../-otp/index.md)? |
| [session](session.md) | [common]<br>var [session](session.md): [Session](../-session/index.md)? |
| [state](state.md) | [common]<br>var [state](state.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? |
| [timestamp](timestamp.md) | [common]<br>var [timestamp](timestamp.md): [Long](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-long/index.html)? |
| [transactionId](transaction-id.md) | [common]<br>var [transactionId](transaction-id.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? |

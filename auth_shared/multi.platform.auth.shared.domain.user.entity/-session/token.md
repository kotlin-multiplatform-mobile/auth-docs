//[auth_shared](../../../index.md)/[multi.platform.auth.shared.domain.user.entity](../index.md)/[Session](index.md)/[token](token.md)

# token

[common]\
var [token](token.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?

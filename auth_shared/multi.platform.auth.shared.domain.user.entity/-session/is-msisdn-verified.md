//[auth_shared](../../../index.md)/[multi.platform.auth.shared.domain.user.entity](../index.md)/[Session](index.md)/[isMsisdnVerified](is-msisdn-verified.md)

# isMsisdnVerified

[common]\
var [isMsisdnVerified](is-msisdn-verified.md): [Boolean](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html)?

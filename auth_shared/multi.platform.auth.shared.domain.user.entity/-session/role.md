//[auth_shared](../../../index.md)/[multi.platform.auth.shared.domain.user.entity](../index.md)/[Session](index.md)/[role](role.md)

# role

[common]\
var [role](role.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?

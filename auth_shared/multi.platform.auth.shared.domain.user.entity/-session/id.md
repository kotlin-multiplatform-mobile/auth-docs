//[auth_shared](../../../index.md)/[multi.platform.auth.shared.domain.user.entity](../index.md)/[Session](index.md)/[id](id.md)

# id

[common]\
var [id](id.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?

//[auth_shared](../../../index.md)/[multi.platform.auth.shared.domain.user.entity](../index.md)/[Session](index.md)/[isAccountVerified](is-account-verified.md)

# isAccountVerified

[common]\
var [isAccountVerified](is-account-verified.md): [Boolean](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html)?

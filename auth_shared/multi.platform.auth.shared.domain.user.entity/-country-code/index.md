//[auth_shared](../../../index.md)/[multi.platform.auth.shared.domain.user.entity](../index.md)/[CountryCode](index.md)

# CountryCode

[common]\
data class [CountryCode](index.md)(val code: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html), val dialCode: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html), val countryName: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html))

## Constructors

| | |
|---|---|
| [CountryCode](-country-code.md) | [common]<br>constructor(code: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html), dialCode: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html), countryName: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)) |

## Properties

| Name | Summary |
|---|---|
| [code](code.md) | [common]<br>val [code](code.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) |
| [countryName](country-name.md) | [common]<br>val [countryName](country-name.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) |
| [dialCode](dial-code.md) | [common]<br>val [dialCode](dial-code.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) |

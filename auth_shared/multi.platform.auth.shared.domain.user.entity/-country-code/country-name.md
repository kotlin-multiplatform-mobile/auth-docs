//[auth_shared](../../../index.md)/[multi.platform.auth.shared.domain.user.entity](../index.md)/[CountryCode](index.md)/[countryName](country-name.md)

# countryName

[common]\
val [countryName](country-name.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)

//[auth_shared](../../../index.md)/[multi.platform.auth.shared.domain.user.entity](../index.md)/[CountryCode](index.md)/[dialCode](dial-code.md)

# dialCode

[common]\
val [dialCode](dial-code.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)

//[auth_shared](../../index.md)/[multi.platform.auth.shared.data.auth](index.md)

# Package-level declarations

## Types

| Name | Summary |
|---|---|
| [AuthRepositoryImpl](-auth-repository-impl/index.md) | [common]<br>class [AuthRepositoryImpl](-auth-repository-impl/index.md)(authConfig: [AuthConfig](../multi.platform.auth.shared.external/-auth-config/index.md), apiClientProvider: ApiClientProvider&lt;HttpClient&gt;) : [AuthRepository](../multi.platform.auth.shared.domain.auth/-auth-repository/index.md) |

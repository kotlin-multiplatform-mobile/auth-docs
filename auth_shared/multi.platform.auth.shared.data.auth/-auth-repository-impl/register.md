//[auth_shared](../../../index.md)/[multi.platform.auth.shared.data.auth](../index.md)/[AuthRepositoryImpl](index.md)/[register](register.md)

# register

[common]\
open suspend override fun [register](register.md)(trxid: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html), userReq: [UserReq](../../multi.platform.auth.shared.data.auth.network.request/-user-req/index.md), imageBytes: [ByteArray](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-byte-array/index.html)?, imageName: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?): JsonObject?

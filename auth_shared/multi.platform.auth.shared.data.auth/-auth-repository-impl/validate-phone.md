//[auth_shared](../../../index.md)/[multi.platform.auth.shared.data.auth](../index.md)/[AuthRepositoryImpl](index.md)/[validatePhone](validate-phone.md)

# validatePhone

[common]\
open suspend override fun [validatePhone](validate-phone.md)(phone: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)): JsonObject?

//[auth_shared](../../../index.md)/[multi.platform.auth.shared.data.auth](../index.md)/[AuthRepositoryImpl](index.md)/[signOut](sign-out.md)

# signOut

[common]\
open suspend override fun [signOut](sign-out.md)(accessToken: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?): JsonObject?

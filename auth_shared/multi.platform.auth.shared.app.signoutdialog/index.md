//[auth_shared](../../index.md)/[multi.platform.auth.shared.app.signoutdialog](index.md)

# Package-level declarations

## Types

| Name | Summary |
|---|---|
| [SignOutDialogFragment](-sign-out-dialog-fragment/index.md) | [android]<br>class [SignOutDialogFragment](-sign-out-dialog-fragment/index.md) : CoreDialogFragment |
| [SignOutViewModel](-sign-out-view-model/index.md) | [common]<br>class [SignOutViewModel](-sign-out-view-model/index.md)(signOutUseCase: [SignOutUseCase](../multi.platform.auth.shared.domain.auth.usecase/-sign-out-use-case/index.md)) : CoreViewModel |

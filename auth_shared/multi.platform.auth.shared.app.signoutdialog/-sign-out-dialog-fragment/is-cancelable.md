//[auth_shared](../../../index.md)/[multi.platform.auth.shared.app.signoutdialog](../index.md)/[SignOutDialogFragment](index.md)/[isCancelable](is-cancelable.md)

# isCancelable

[android]\
open override fun [isCancelable](is-cancelable.md)(): [Boolean](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html)

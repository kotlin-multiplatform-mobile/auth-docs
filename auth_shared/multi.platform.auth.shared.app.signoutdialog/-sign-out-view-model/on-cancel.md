//[auth_shared](../../../index.md)/[multi.platform.auth.shared.app.signoutdialog](../index.md)/[SignOutViewModel](index.md)/[onCancel](on-cancel.md)

# onCancel

[common]\
val [onCancel](on-cancel.md): MutableStateFlow&lt;[Boolean](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html)&gt;

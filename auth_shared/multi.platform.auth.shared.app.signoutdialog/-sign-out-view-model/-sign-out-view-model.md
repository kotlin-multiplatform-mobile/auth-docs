//[auth_shared](../../../index.md)/[multi.platform.auth.shared.app.signoutdialog](../index.md)/[SignOutViewModel](index.md)/[SignOutViewModel](-sign-out-view-model.md)

# SignOutViewModel

[common]\
constructor(signOutUseCase: [SignOutUseCase](../../multi.platform.auth.shared.domain.auth.usecase/-sign-out-use-case/index.md))

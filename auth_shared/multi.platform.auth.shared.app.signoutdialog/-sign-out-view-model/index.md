//[auth_shared](../../../index.md)/[multi.platform.auth.shared.app.signoutdialog](../index.md)/[SignOutViewModel](index.md)

# SignOutViewModel

[common]\
class [SignOutViewModel](index.md)(signOutUseCase: [SignOutUseCase](../../multi.platform.auth.shared.domain.auth.usecase/-sign-out-use-case/index.md)) : CoreViewModel

## Constructors

| | |
|---|---|
| [SignOutViewModel](-sign-out-view-model.md) | [common]<br>constructor(signOutUseCase: [SignOutUseCase](../../multi.platform.auth.shared.domain.auth.usecase/-sign-out-use-case/index.md)) |

## Properties

| Name | Summary |
|---|---|
| [accessToken](index.md#-74023671%2FProperties%2F-1413040735) | [common]<br>expect var [accessToken](index.md#-74023671%2FProperties%2F-1413040735): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? |
| [errorConfirm](index.md#1488355492%2FProperties%2F-1413040735) | [common]<br>expect var [errorConfirm](index.md#1488355492%2FProperties%2F-1413040735): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? |
| [errorEmailFormat](index.md#-620529519%2FProperties%2F-1413040735) | [common]<br>expect var [errorEmailFormat](index.md#-620529519%2FProperties%2F-1413040735): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? |
| [errorEmptyField](index.md#832052137%2FProperties%2F-1413040735) | [common]<br>expect var [errorEmptyField](index.md#832052137%2FProperties%2F-1413040735): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? |
| [errorMessage](index.md#-366963395%2FProperties%2F-1413040735) | [common]<br>expect val [errorMessage](index.md#-366963395%2FProperties%2F-1413040735): MutableStateFlow&lt;[String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?&gt; |
| [errorMinChar](index.md#-784102500%2FProperties%2F-1413040735) | [common]<br>expect var [errorMinChar](index.md#-784102500%2FProperties%2F-1413040735): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? |
| [errorPasswordFormat](index.md#265462788%2FProperties%2F-1413040735) | [common]<br>expect var [errorPasswordFormat](index.md#265462788%2FProperties%2F-1413040735): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? |
| [errorPhoneFormat](index.md#-350172033%2FProperties%2F-1413040735) | [common]<br>expect var [errorPhoneFormat](index.md#-350172033%2FProperties%2F-1413040735): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? |
| [forceSignout](index.md#2001686774%2FProperties%2F-1413040735) | [common]<br>expect val [forceSignout](index.md#2001686774%2FProperties%2F-1413040735): MutableStateFlow&lt;[Boolean](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html)&gt; |
| [isEmpty](index.md#2032650747%2FProperties%2F-1413040735) | [common]<br>expect val [isEmpty](index.md#2032650747%2FProperties%2F-1413040735): MutableStateFlow&lt;[Boolean](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html)&gt; |
| [isFromNetwork](index.md#450452420%2FProperties%2F-1413040735) | [common]<br>expect var [isFromNetwork](index.md#450452420%2FProperties%2F-1413040735): [Boolean](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html) |
| [loadingIndicator](index.md#-315772695%2FProperties%2F-1413040735) | [common]<br>expect val [loadingIndicator](index.md#-315772695%2FProperties%2F-1413040735): MutableStateFlow&lt;[Boolean](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html)?&gt; |
| [onCancel](on-cancel.md) | [common]<br>val [onCancel](on-cancel.md): MutableStateFlow&lt;[Boolean](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html)&gt; |
| [onException](index.md#-2065585362%2FProperties%2F-1413040735) | [common]<br>expect val [onException](index.md#-2065585362%2FProperties%2F-1413040735): MutableStateFlow&lt;[Exception](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-exception/index.html)?&gt; |
| [onSignOut](on-sign-out.md) | [common]<br>val [onSignOut](on-sign-out.md): MutableStateFlow&lt;[Boolean](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html)&gt; |
| [scope](index.md#1071716170%2FProperties%2F-1413040735) | [common]<br>expect val [scope](index.md#1071716170%2FProperties%2F-1413040735): CoroutineScope |
| [successMessage](index.md#-428149896%2FProperties%2F-1413040735) | [common]<br>expect val [successMessage](index.md#-428149896%2FProperties%2F-1413040735): MutableStateFlow&lt;[String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?&gt; |
| [toastMessage](index.md#-805167780%2FProperties%2F-1413040735) | [common]<br>expect val [toastMessage](index.md#-805167780%2FProperties%2F-1413040735): MutableStateFlow&lt;[String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?&gt; |
| [useAsyncNetworkCall](index.md#1408937991%2FProperties%2F-1413040735) | [common]<br>expect var [useAsyncNetworkCall](index.md#1408937991%2FProperties%2F-1413040735): [Boolean](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html) |

## Functions

| Name | Summary |
|---|---|
| [cancel](cancel.md) | [common]<br>fun [cancel](cancel.md)() |
| [signOut](sign-out.md) | [common]<br>fun [signOut](sign-out.md)() |
| [validateBlank](index.md#-597343809%2FFunctions%2F-1413040735) | [common]<br>expect fun [validateBlank](index.md#-597343809%2FFunctions%2F-1413040735)(field: MutableStateFlow&lt;[String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?&gt;, error: MutableStateFlow&lt;[String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?&gt;): [Boolean](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html) |
| [validateConfirm](index.md#-944246634%2FFunctions%2F-1413040735) | [common]<br>expect fun [validateConfirm](index.md#-944246634%2FFunctions%2F-1413040735)(field1: MutableStateFlow&lt;[String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?&gt;, field2: MutableStateFlow&lt;[String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?&gt;, error: MutableStateFlow&lt;[String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?&gt;): [Boolean](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html) |
| [validateEmailFormat](index.md#52700574%2FFunctions%2F-1413040735) | [common]<br>expect fun [validateEmailFormat](index.md#52700574%2FFunctions%2F-1413040735)(field: MutableStateFlow&lt;[String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?&gt;, error: MutableStateFlow&lt;[String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?&gt;): [Boolean](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html) |
| [validateMinChar](index.md#2003153546%2FFunctions%2F-1413040735) | [common]<br>expect fun [validateMinChar](index.md#2003153546%2FFunctions%2F-1413040735)(min: [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html), field: MutableStateFlow&lt;[String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?&gt;, error: MutableStateFlow&lt;[String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?&gt;): [Boolean](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html) |
| [validatePasswordFormat](index.md#1543656427%2FFunctions%2F-1413040735) | [common]<br>expect fun [validatePasswordFormat](index.md#1543656427%2FFunctions%2F-1413040735)(field: MutableStateFlow&lt;[String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?&gt;, error: MutableStateFlow&lt;[String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?&gt;): [Boolean](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html) |
| [validatePhoneFormat](index.md#2112099184%2FFunctions%2F-1413040735) | [common]<br>expect fun [validatePhoneFormat](index.md#2112099184%2FFunctions%2F-1413040735)(field: MutableStateFlow&lt;[String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?&gt;, error: MutableStateFlow&lt;[String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?&gt;): [Boolean](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html) |

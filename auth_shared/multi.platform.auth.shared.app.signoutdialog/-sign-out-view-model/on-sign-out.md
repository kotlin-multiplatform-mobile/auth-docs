//[auth_shared](../../../index.md)/[multi.platform.auth.shared.app.signoutdialog](../index.md)/[SignOutViewModel](index.md)/[onSignOut](on-sign-out.md)

# onSignOut

[common]\
val [onSignOut](on-sign-out.md): MutableStateFlow&lt;[Boolean](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html)&gt;

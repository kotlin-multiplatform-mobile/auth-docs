//[auth_shared](../../../index.md)/[multi.platform.auth.shared.data.auth.network.request](../index.md)/[SignInByPhoneReq](index.md)/[msisdn](msisdn.md)

# msisdn

[common]\
val [msisdn](msisdn.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? = null

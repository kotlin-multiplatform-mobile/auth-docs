//[auth_shared](../../../index.md)/[multi.platform.auth.shared.data.auth.network.request](../index.md)/[SignInByPhoneReq](index.md)

# SignInByPhoneReq

[common]\
@Serializable

data class [SignInByPhoneReq](index.md)(val msisdn: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? = null)

## Constructors

| | |
|---|---|
| [SignInByPhoneReq](-sign-in-by-phone-req.md) | [common]<br>constructor(msisdn: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? = null) |

## Properties

| Name | Summary |
|---|---|
| [msisdn](msisdn.md) | [common]<br>val [msisdn](msisdn.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? = null |

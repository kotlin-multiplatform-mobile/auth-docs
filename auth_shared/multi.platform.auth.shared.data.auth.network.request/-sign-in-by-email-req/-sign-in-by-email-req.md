//[auth_shared](../../../index.md)/[multi.platform.auth.shared.data.auth.network.request](../index.md)/[SignInByEmailReq](index.md)/[SignInByEmailReq](-sign-in-by-email-req.md)

# SignInByEmailReq

[common]\
constructor(email: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? = null, password: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? = null)

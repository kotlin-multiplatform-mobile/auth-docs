//[auth_shared](../../../index.md)/[multi.platform.auth.shared.data.auth.network.request](../index.md)/[SigninProviderReq](index.md)/[subscriberAccess](subscriber-access.md)

# subscriberAccess

[common]\
val [subscriberAccess](subscriber-access.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)

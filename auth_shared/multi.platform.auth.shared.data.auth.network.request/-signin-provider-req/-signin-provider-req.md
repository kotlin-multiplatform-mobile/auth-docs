//[auth_shared](../../../index.md)/[multi.platform.auth.shared.data.auth.network.request](../index.md)/[SigninProviderReq](index.md)/[SigninProviderReq](-signin-provider-req.md)

# SigninProviderReq

[common]\
constructor(provider: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? = null, email: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? = null, fullname: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? = null, subscriberAccess: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html))

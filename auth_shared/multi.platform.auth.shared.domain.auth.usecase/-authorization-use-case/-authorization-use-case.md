//[auth_shared](../../../index.md)/[multi.platform.auth.shared.domain.auth.usecase](../index.md)/[AuthorizationUseCase](index.md)/[AuthorizationUseCase](-authorization-use-case.md)

# AuthorizationUseCase

[common]\
constructor(authConfig: [AuthConfig](../../multi.platform.auth.shared.external/-auth-config/index.md), authRepository: [AuthRepository](../../multi.platform.auth.shared.domain.auth/-auth-repository/index.md))

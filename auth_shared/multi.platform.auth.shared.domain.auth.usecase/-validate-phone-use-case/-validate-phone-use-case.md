//[auth_shared](../../../index.md)/[multi.platform.auth.shared.domain.auth.usecase](../index.md)/[ValidatePhoneUseCase](index.md)/[ValidatePhoneUseCase](-validate-phone-use-case.md)

# ValidatePhoneUseCase

[common]\
constructor(authConfig: [AuthConfig](../../multi.platform.auth.shared.external/-auth-config/index.md), authRepository: [AuthRepository](../../multi.platform.auth.shared.domain.auth/-auth-repository/index.md))

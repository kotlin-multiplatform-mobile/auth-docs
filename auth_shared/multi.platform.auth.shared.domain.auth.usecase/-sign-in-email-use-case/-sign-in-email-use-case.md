//[auth_shared](../../../index.md)/[multi.platform.auth.shared.domain.auth.usecase](../index.md)/[SignInEmailUseCase](index.md)/[SignInEmailUseCase](-sign-in-email-use-case.md)

# SignInEmailUseCase

[common]\
constructor(authConfig: [AuthConfig](../../multi.platform.auth.shared.external/-auth-config/index.md), authRepository: [AuthRepository](../../multi.platform.auth.shared.domain.auth/-auth-repository/index.md))

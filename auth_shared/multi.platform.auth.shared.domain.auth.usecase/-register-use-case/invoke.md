//[auth_shared](../../../index.md)/[multi.platform.auth.shared.domain.auth.usecase](../index.md)/[RegisterUseCase](index.md)/[invoke](invoke.md)

# invoke

[common]\
suspend operator fun [invoke](invoke.md)(trxid: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html), userReq: [UserReq](../../multi.platform.auth.shared.data.auth.network.request/-user-req/index.md), imageBytes: [ByteArray](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-byte-array/index.html)?, imageName: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?): [Ticket](../../multi.platform.auth.shared.domain.auth.entity/-ticket/index.md)?

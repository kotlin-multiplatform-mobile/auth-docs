//[auth_shared](../../../index.md)/[multi.platform.auth.shared.domain.auth.usecase](../index.md)/[RegisterUseCase](index.md)

# RegisterUseCase

[common]\
class [RegisterUseCase](index.md)(authConfig: [AuthConfig](../../multi.platform.auth.shared.external/-auth-config/index.md), authRepository: [AuthRepository](../../multi.platform.auth.shared.domain.auth/-auth-repository/index.md))

## Constructors

| | |
|---|---|
| [RegisterUseCase](-register-use-case.md) | [common]<br>constructor(authConfig: [AuthConfig](../../multi.platform.auth.shared.external/-auth-config/index.md), authRepository: [AuthRepository](../../multi.platform.auth.shared.domain.auth/-auth-repository/index.md)) |

## Functions

| Name | Summary |
|---|---|
| [invoke](invoke.md) | [common]<br>suspend operator fun [invoke](invoke.md)(trxid: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html), userReq: [UserReq](../../multi.platform.auth.shared.data.auth.network.request/-user-req/index.md), imageBytes: [ByteArray](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-byte-array/index.html)?, imageName: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?): [Ticket](../../multi.platform.auth.shared.domain.auth.entity/-ticket/index.md)? |

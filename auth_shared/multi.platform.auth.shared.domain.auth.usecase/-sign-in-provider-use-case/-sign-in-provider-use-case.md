//[auth_shared](../../../index.md)/[multi.platform.auth.shared.domain.auth.usecase](../index.md)/[SignInProviderUseCase](index.md)/[SignInProviderUseCase](-sign-in-provider-use-case.md)

# SignInProviderUseCase

[common]\
constructor(authConfig: [AuthConfig](../../multi.platform.auth.shared.external/-auth-config/index.md), authRepository: [AuthRepository](../../multi.platform.auth.shared.domain.auth/-auth-repository/index.md))

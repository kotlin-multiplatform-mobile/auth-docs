//[auth_shared](../../../index.md)/[multi.platform.auth.shared.domain.auth.usecase](../index.md)/[VerifyOtpUseCase](index.md)/[invoke](invoke.md)

# invoke

[common]\
suspend operator fun [invoke](invoke.md)(otp: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html), type: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html), phone: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)): [Ticket](../../multi.platform.auth.shared.domain.auth.entity/-ticket/index.md)?

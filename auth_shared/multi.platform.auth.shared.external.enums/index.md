//[auth_shared](../../index.md)/[multi.platform.auth.shared.external.enums](index.md)

# Package-level declarations

## Types

| Name | Summary |
|---|---|
| [AuthType](-auth-type/index.md) | [common]<br>enum [AuthType](-auth-type/index.md) : [Enum](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-enum/index.html)&lt;[AuthType](-auth-type/index.md)&gt; |
| [UserState](-user-state/index.md) | [common]<br>enum [UserState](-user-state/index.md) : [Enum](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-enum/index.html)&lt;[UserState](-user-state/index.md)&gt; |

//[auth_shared](../../../../index.md)/[multi.platform.auth.shared.external.enums](../../index.md)/[AuthType](../index.md)/[BIOMETRIC](index.md)

# BIOMETRIC

[common]\
[BIOMETRIC](index.md)

## Properties

| Name | Summary |
|---|---|
| [name](../../-user-state/-u-n-d-e-f-i-n-e-d/index.md#-372974862%2FProperties%2F-1413040735) | [common]<br>val [name](../../-user-state/-u-n-d-e-f-i-n-e-d/index.md#-372974862%2FProperties%2F-1413040735): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) |
| [ordinal](../../-user-state/-u-n-d-e-f-i-n-e-d/index.md#-739389684%2FProperties%2F-1413040735) | [common]<br>val [ordinal](../../-user-state/-u-n-d-e-f-i-n-e-d/index.md#-739389684%2FProperties%2F-1413040735): [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html) |

//[auth_shared](../../../index.md)/[multi.platform.auth.shared.external.enums](../index.md)/[AuthType](index.md)

# AuthType

[common]\
enum [AuthType](index.md) : [Enum](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-enum/index.html)&lt;[AuthType](index.md)&gt;

## Entries

| | |
|---|---|
| [PHONE](-p-h-o-n-e/index.md) | [common]<br>[PHONE](-p-h-o-n-e/index.md) |
| [EMAIL](-e-m-a-i-l/index.md) | [common]<br>[EMAIL](-e-m-a-i-l/index.md) |
| [GOOGLE](-g-o-o-g-l-e/index.md) | [common]<br>[GOOGLE](-g-o-o-g-l-e/index.md) |
| [FACEBOOK](-f-a-c-e-b-o-o-k/index.md) | [common]<br>[FACEBOOK](-f-a-c-e-b-o-o-k/index.md) |
| [BIOMETRIC](-b-i-o-m-e-t-r-i-c/index.md) | [common]<br>[BIOMETRIC](-b-i-o-m-e-t-r-i-c/index.md) |
| [UNDEFINED](-u-n-d-e-f-i-n-e-d/index.md) | [common]<br>[UNDEFINED](-u-n-d-e-f-i-n-e-d/index.md) |

## Properties

| Name | Summary |
|---|---|
| [entries](entries.md) | [common]<br>val [entries](entries.md): [EnumEntries](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin.enums/-enum-entries/index.html)&lt;[AuthType](index.md)&gt;<br>Returns a representation of an immutable list of all enum entries, in the order they're declared. |
| [name](../-user-state/-u-n-d-e-f-i-n-e-d/index.md#-372974862%2FProperties%2F-1413040735) | [common]<br>val [name](../-user-state/-u-n-d-e-f-i-n-e-d/index.md#-372974862%2FProperties%2F-1413040735): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) |
| [ordinal](../-user-state/-u-n-d-e-f-i-n-e-d/index.md#-739389684%2FProperties%2F-1413040735) | [common]<br>val [ordinal](../-user-state/-u-n-d-e-f-i-n-e-d/index.md#-739389684%2FProperties%2F-1413040735): [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html) |

## Functions

| Name | Summary |
|---|---|
| [valueOf](value-of.md) | [common]<br>fun [valueOf](value-of.md)(value: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)): [AuthType](index.md)<br>Returns the enum constant of this type with the specified name. The string must match exactly an identifier used to declare an enum constant in this type. (Extraneous whitespace characters are not permitted.) |
| [values](values.md) | [common]<br>fun [values](values.md)(): [Array](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-array/index.html)&lt;[AuthType](index.md)&gt;<br>Returns an array containing the constants of this enum type, in the order they're declared. |

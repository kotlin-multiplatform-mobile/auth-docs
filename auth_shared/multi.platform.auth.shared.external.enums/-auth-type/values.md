//[auth_shared](../../../index.md)/[multi.platform.auth.shared.external.enums](../index.md)/[AuthType](index.md)/[values](values.md)

# values

[common]\
fun [values](values.md)(): [Array](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-array/index.html)&lt;[AuthType](index.md)&gt;

Returns an array containing the constants of this enum type, in the order they're declared.

This method may be used to iterate over the constants.

//[auth_shared](../../../../index.md)/[multi.platform.auth.shared.external.enums](../../index.md)/[UserState](../index.md)/[UNDEFINED](index.md)

# UNDEFINED

[common]\
[UNDEFINED](index.md)

## Properties

| Name | Summary |
|---|---|
| [name](index.md#-372974862%2FProperties%2F-1413040735) | [common]<br>val [name](index.md#-372974862%2FProperties%2F-1413040735): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) |
| [ordinal](index.md#-739389684%2FProperties%2F-1413040735) | [common]<br>val [ordinal](index.md#-739389684%2FProperties%2F-1413040735): [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html) |

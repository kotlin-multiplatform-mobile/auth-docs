//[auth_shared](../../../index.md)/[multi.platform.auth.shared.external.enums](../index.md)/[UserState](index.md)

# UserState

[common]\
enum [UserState](index.md) : [Enum](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-enum/index.html)&lt;[UserState](index.md)&gt;

## Entries

| | |
|---|---|
| [VALIDATED](-v-a-l-i-d-a-t-e-d/index.md) | [common]<br>[VALIDATED](-v-a-l-i-d-a-t-e-d/index.md) |
| [SECURED](-s-e-c-u-r-e-d/index.md) | [common]<br>[SECURED](-s-e-c-u-r-e-d/index.md) |
| [REGISTERED](-r-e-g-i-s-t-e-r-e-d/index.md) | [common]<br>[REGISTERED](-r-e-g-i-s-t-e-r-e-d/index.md) |
| [ACTIVATED](-a-c-t-i-v-a-t-e-d/index.md) | [common]<br>[ACTIVATED](-a-c-t-i-v-a-t-e-d/index.md) |
| [UNDEFINED](-u-n-d-e-f-i-n-e-d/index.md) | [common]<br>[UNDEFINED](-u-n-d-e-f-i-n-e-d/index.md) |

## Properties

| Name | Summary |
|---|---|
| [entries](entries.md) | [common]<br>val [entries](entries.md): [EnumEntries](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin.enums/-enum-entries/index.html)&lt;[UserState](index.md)&gt;<br>Returns a representation of an immutable list of all enum entries, in the order they're declared. |
| [name](-u-n-d-e-f-i-n-e-d/index.md#-372974862%2FProperties%2F-1413040735) | [common]<br>val [name](-u-n-d-e-f-i-n-e-d/index.md#-372974862%2FProperties%2F-1413040735): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) |
| [ordinal](-u-n-d-e-f-i-n-e-d/index.md#-739389684%2FProperties%2F-1413040735) | [common]<br>val [ordinal](-u-n-d-e-f-i-n-e-d/index.md#-739389684%2FProperties%2F-1413040735): [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html) |

## Functions

| Name | Summary |
|---|---|
| [valueOf](value-of.md) | [common]<br>fun [valueOf](value-of.md)(value: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)): [UserState](index.md)<br>Returns the enum constant of this type with the specified name. The string must match exactly an identifier used to declare an enum constant in this type. (Extraneous whitespace characters are not permitted.) |
| [values](values.md) | [common]<br>fun [values](values.md)(): [Array](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-array/index.html)&lt;[UserState](index.md)&gt;<br>Returns an array containing the constants of this enum type, in the order they're declared. |

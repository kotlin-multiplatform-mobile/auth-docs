//[auth_shared](../../../index.md)/[multi.platform.auth.shared.app.forgetpassworddialog](../index.md)/[ForgetPasswordDialogFragment](index.md)/[onCreate](on-create.md)

# onCreate

[android]\
open override fun [onCreate](on-create.md)(savedInstanceState: [Bundle](https://developer.android.com/reference/kotlin/android/os/Bundle.html)?)

//[auth_shared](../../index.md)/[multi.platform.auth.shared.app.forgetpassworddialog](index.md)

# Package-level declarations

## Types

| Name | Summary |
|---|---|
| [ForgetPasswordDialogFragment](-forget-password-dialog-fragment/index.md) | [android]<br>class [ForgetPasswordDialogFragment](-forget-password-dialog-fragment/index.md) : CoreDialogFragment |
| [ForgetPasswordViewModel](-forget-password-view-model/index.md) | [common]<br>class [ForgetPasswordViewModel](-forget-password-view-model/index.md)(forgetPasswordUseCase: [ForgetPasswordUseCase](../multi.platform.auth.shared.domain.auth.usecase/-forget-password-use-case/index.md)) : CoreViewModel |

//[auth_shared](../../../index.md)/[multi.platform.auth.shared.app.forgetpassworddialog](../index.md)/[ForgetPasswordViewModel](index.md)/[ForgetPasswordViewModel](-forget-password-view-model.md)

# ForgetPasswordViewModel

[common]\
constructor(forgetPasswordUseCase: [ForgetPasswordUseCase](../../multi.platform.auth.shared.domain.auth.usecase/-forget-password-use-case/index.md))

//[auth_shared](../../../index.md)/[multi.platform.auth.shared.app.forgetpassworddialog](../index.md)/[ForgetPasswordViewModel](index.md)/[onSubmit](on-submit.md)

# onSubmit

[common]\
val [onSubmit](on-submit.md): MutableStateFlow&lt;[Boolean](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html)&gt;

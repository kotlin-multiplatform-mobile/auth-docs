//[auth_shared](../../../index.md)/[multi.platform.auth.shared.app.forgetpassworddialog](../index.md)/[ForgetPasswordViewModel](index.md)/[emailError](email-error.md)

# emailError

[common]\
val [emailError](email-error.md): MutableStateFlow&lt;[String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?&gt;

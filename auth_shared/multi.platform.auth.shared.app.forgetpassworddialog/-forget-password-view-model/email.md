//[auth_shared](../../../index.md)/[multi.platform.auth.shared.app.forgetpassworddialog](../index.md)/[ForgetPasswordViewModel](index.md)/[email](email.md)

# email

[common]\
val [email](email.md): MutableStateFlow&lt;[String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?&gt;

//[auth_shared](../../index.md)/[multi.platform.auth.shared.domain.auth](index.md)

# Package-level declarations

## Types

| Name | Summary |
|---|---|
| [AuthRepository](-auth-repository/index.md) | [common]<br>interface [AuthRepository](-auth-repository/index.md) |

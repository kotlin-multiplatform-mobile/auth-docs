//[auth_shared](../../../index.md)/[multi.platform.auth.shared.domain.auth](../index.md)/[AuthRepository](index.md)/[signInEmail](sign-in-email.md)

# signInEmail

[common]\
abstract suspend fun [signInEmail](sign-in-email.md)(email: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html), password: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)): JsonObject?

//[auth_shared](../../../index.md)/[multi.platform.auth.shared.domain.auth](../index.md)/[AuthRepository](index.md)/[signInProvider](sign-in-provider.md)

# signInProvider

[common]\
abstract suspend fun [signInProvider](sign-in-provider.md)(authType: [AuthType](../../multi.platform.auth.shared.external.enums/-auth-type/index.md), token: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html), userReq: [UserReq](../../multi.platform.auth.shared.data.auth.network.request/-user-req/index.md)?): JsonObject?

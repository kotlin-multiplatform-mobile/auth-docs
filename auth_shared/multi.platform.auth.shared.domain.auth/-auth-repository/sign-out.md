//[auth_shared](../../../index.md)/[multi.platform.auth.shared.domain.auth](../index.md)/[AuthRepository](index.md)/[signOut](sign-out.md)

# signOut

[common]\
abstract suspend fun [signOut](sign-out.md)(accessToken: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?): JsonObject?

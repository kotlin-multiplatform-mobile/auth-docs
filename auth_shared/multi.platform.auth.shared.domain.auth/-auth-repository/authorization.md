//[auth_shared](../../../index.md)/[multi.platform.auth.shared.domain.auth](../index.md)/[AuthRepository](index.md)/[authorization](authorization.md)

# authorization

[common]\
abstract suspend fun [authorization](authorization.md)(phone: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)): JsonObject?

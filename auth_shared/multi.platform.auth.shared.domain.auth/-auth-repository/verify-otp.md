//[auth_shared](../../../index.md)/[multi.platform.auth.shared.domain.auth](../index.md)/[AuthRepository](index.md)/[verifyOtp](verify-otp.md)

# verifyOtp

[common]\
abstract suspend fun [verifyOtp](verify-otp.md)(otp: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html), type: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html), phone: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)): JsonObject?

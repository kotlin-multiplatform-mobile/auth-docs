//[auth_shared](../../../index.md)/[multi.platform.auth.shared.app.otpdialog](../index.md)/[OtpViewModel](index.md)/[OtpViewModel](-otp-view-model.md)

# OtpViewModel

[common]\
constructor(authorizationUseCase: [AuthorizationUseCase](../../multi.platform.auth.shared.domain.auth.usecase/-authorization-use-case/index.md), verifyOtpUseCase: [VerifyOtpUseCase](../../multi.platform.auth.shared.domain.auth.usecase/-verify-otp-use-case/index.md))

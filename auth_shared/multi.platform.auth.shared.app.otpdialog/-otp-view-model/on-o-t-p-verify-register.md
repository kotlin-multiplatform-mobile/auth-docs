//[auth_shared](../../../index.md)/[multi.platform.auth.shared.app.otpdialog](../index.md)/[OtpViewModel](index.md)/[onOTPVerifyRegister](on-o-t-p-verify-register.md)

# onOTPVerifyRegister

[common]\
val [onOTPVerifyRegister](on-o-t-p-verify-register.md): MutableStateFlow&lt;[Ticket](../../multi.platform.auth.shared.domain.auth.entity/-ticket/index.md)?&gt;

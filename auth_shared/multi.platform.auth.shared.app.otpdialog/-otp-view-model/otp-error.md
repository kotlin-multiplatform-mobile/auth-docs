//[auth_shared](../../../index.md)/[multi.platform.auth.shared.app.otpdialog](../index.md)/[OtpViewModel](index.md)/[otpError](otp-error.md)

# otpError

[common]\
val [otpError](otp-error.md): MutableStateFlow&lt;[String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?&gt;

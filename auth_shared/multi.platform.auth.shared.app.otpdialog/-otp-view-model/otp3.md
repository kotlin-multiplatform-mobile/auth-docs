//[auth_shared](../../../index.md)/[multi.platform.auth.shared.app.otpdialog](../index.md)/[OtpViewModel](index.md)/[otp3](otp3.md)

# otp3

[common]\
val [otp3](otp3.md): MutableStateFlow&lt;[String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?&gt;

//[auth_shared](../../../index.md)/[multi.platform.auth.shared.app.otpdialog](../index.md)/[OtpViewModel](index.md)/[verifyOtp](verify-otp.md)

# verifyOtp

[common]\
fun [verifyOtp](verify-otp.md)(otp: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html))

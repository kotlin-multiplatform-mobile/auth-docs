//[auth_shared](../../../index.md)/[multi.platform.auth.shared.app.otpdialog](../index.md)/[OtpViewModel](index.md)/[phone](phone.md)

# phone

[common]\
val [phone](phone.md): MutableStateFlow&lt;[String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?&gt;

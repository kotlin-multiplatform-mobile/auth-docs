//[auth_shared](../../../index.md)/[multi.platform.auth.shared.app.otpdialog](../index.md)/[OtpViewModel](index.md)/[otpResend](otp-resend.md)

# otpResend

[common]\
val [otpResend](otp-resend.md): MutableStateFlow&lt;[String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?&gt;

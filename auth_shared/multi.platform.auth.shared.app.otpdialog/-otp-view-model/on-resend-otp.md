//[auth_shared](../../../index.md)/[multi.platform.auth.shared.app.otpdialog](../index.md)/[OtpViewModel](index.md)/[onResendOtp](on-resend-otp.md)

# onResendOtp

[common]\
val [onResendOtp](on-resend-otp.md): MutableStateFlow&lt;[Ticket](../../multi.platform.auth.shared.domain.auth.entity/-ticket/index.md)?&gt;

//[auth_shared](../../index.md)/[multi.platform.auth.shared.app.otpdialog](index.md)

# Package-level declarations

## Types

| Name | Summary |
|---|---|
| [OtpDialogFragment](-otp-dialog-fragment/index.md) | [android]<br>class [OtpDialogFragment](-otp-dialog-fragment/index.md) : CoreDialogFragment |
| [OtpViewModel](-otp-view-model/index.md) | [common]<br>class [OtpViewModel](-otp-view-model/index.md)(authorizationUseCase: [AuthorizationUseCase](../multi.platform.auth.shared.domain.auth.usecase/-authorization-use-case/index.md), verifyOtpUseCase: [VerifyOtpUseCase](../multi.platform.auth.shared.domain.auth.usecase/-verify-otp-use-case/index.md)) : CoreViewModel |

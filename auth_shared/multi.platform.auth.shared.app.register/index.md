//[auth_shared](../../index.md)/[multi.platform.auth.shared.app.register](index.md)

# Package-level declarations

## Types

| Name | Summary |
|---|---|
| [RegisterFragment](-register-fragment/index.md) | [android]<br>class [RegisterFragment](-register-fragment/index.md) : CoreFragment |
| [RegisterViewModel](-register-view-model/index.md) | [common]<br>class [RegisterViewModel](-register-view-model/index.md)(registerUseCase: [RegisterUseCase](../multi.platform.auth.shared.domain.auth.usecase/-register-use-case/index.md)) : CoreViewModel |

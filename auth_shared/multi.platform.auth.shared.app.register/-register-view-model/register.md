//[auth_shared](../../../index.md)/[multi.platform.auth.shared.app.register](../index.md)/[RegisterViewModel](index.md)/[register](register.md)

# register

[common]\
fun [register](register.md)(imageBytes: [ByteArray](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-byte-array/index.html)?, imageName: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?)

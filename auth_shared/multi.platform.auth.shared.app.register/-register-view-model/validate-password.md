//[auth_shared](../../../index.md)/[multi.platform.auth.shared.app.register](../index.md)/[RegisterViewModel](index.md)/[validatePassword](validate-password.md)

# validatePassword

[common]\
fun [validatePassword](validate-password.md)(): [Boolean](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html)?

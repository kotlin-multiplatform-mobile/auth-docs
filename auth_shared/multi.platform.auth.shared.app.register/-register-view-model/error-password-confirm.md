//[auth_shared](../../../index.md)/[multi.platform.auth.shared.app.register](../index.md)/[RegisterViewModel](index.md)/[errorPasswordConfirm](error-password-confirm.md)

# errorPasswordConfirm

[common]\
var [errorPasswordConfirm](error-password-confirm.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?

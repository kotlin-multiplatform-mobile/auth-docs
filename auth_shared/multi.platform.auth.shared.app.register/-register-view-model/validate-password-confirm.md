//[auth_shared](../../../index.md)/[multi.platform.auth.shared.app.register](../index.md)/[RegisterViewModel](index.md)/[validatePasswordConfirm](validate-password-confirm.md)

# validatePasswordConfirm

[common]\
fun [validatePasswordConfirm](validate-password-confirm.md)(): [Boolean](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html)?

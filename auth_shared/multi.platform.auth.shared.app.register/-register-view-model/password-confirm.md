//[auth_shared](../../../index.md)/[multi.platform.auth.shared.app.register](../index.md)/[RegisterViewModel](index.md)/[passwordConfirm](password-confirm.md)

# passwordConfirm

[common]\
val [passwordConfirm](password-confirm.md): MutableStateFlow&lt;[String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?&gt;

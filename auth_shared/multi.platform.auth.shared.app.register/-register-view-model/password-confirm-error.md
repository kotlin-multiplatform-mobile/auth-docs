//[auth_shared](../../../index.md)/[multi.platform.auth.shared.app.register](../index.md)/[RegisterViewModel](index.md)/[passwordConfirmError](password-confirm-error.md)

# passwordConfirmError

[common]\
val [passwordConfirmError](password-confirm-error.md): MutableStateFlow&lt;[String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?&gt;

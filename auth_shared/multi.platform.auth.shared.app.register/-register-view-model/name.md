//[auth_shared](../../../index.md)/[multi.platform.auth.shared.app.register](../index.md)/[RegisterViewModel](index.md)/[name](name.md)

# name

[common]\
val [name](name.md): MutableStateFlow&lt;[String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?&gt;

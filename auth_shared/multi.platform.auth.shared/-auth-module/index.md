//[auth_shared](../../../index.md)/[multi.platform.auth.shared](../index.md)/[AuthModule](index.md)

# AuthModule

[common]\
class [AuthModule](index.md)(authConfig: [AuthConfig](../../multi.platform.auth.shared.external/-auth-config/index.md))

## Constructors

| | |
|---|---|
| [AuthModule](-auth-module.md) | [common]<br>constructor(authConfig: [AuthConfig](../../multi.platform.auth.shared.external/-auth-config/index.md)) |

## Functions

| Name | Summary |
|---|---|
| [invoke](invoke.md) | [common]<br>operator fun [invoke](invoke.md)(): Module |

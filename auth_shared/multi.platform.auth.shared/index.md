//[auth_shared](../../index.md)/[multi.platform.auth.shared](index.md)

# Package-level declarations

## Types

| Name | Summary |
|---|---|
| [AuthModule](-auth-module/index.md) | [common]<br>class [AuthModule](-auth-module/index.md)(authConfig: [AuthConfig](../multi.platform.auth.shared.external/-auth-config/index.md)) |

## Functions

| Name | Summary |
|---|---|
| [authModule](auth-module.md) | [common, android, ios]<br>[common]<br>expect fun [authModule](auth-module.md)(): Module<br>[android, ios]<br>actual fun [authModule](auth-module.md)(): Module |

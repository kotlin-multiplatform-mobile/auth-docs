//[auth_shared](../../index.md)/[multi.platform.auth.shared](index.md)/[authModule](auth-module.md)

# authModule

[common, android, ios]\
[common]\
expect fun [authModule](auth-module.md)(): Module

[android, ios]\
actual fun [authModule](auth-module.md)(): Module

//[auth_shared](../../index.md)/[multi.platform.auth.shared.domain.user.usecase](index.md)

# Package-level declarations

## Types

| Name | Summary |
|---|---|
| [AuthorizationUseCase](-authorization-use-case/index.md) | [common]<br>class [AuthorizationUseCase](-authorization-use-case/index.md)(authConfig: [AuthConfig](../multi.platform.auth.shared.external/-auth-config/index.md), userRepository: [UserRepository](../multi.platform.auth.shared.domain.user/-user-repository/index.md)) |
| [ForgetPasswordUseCase](-forget-password-use-case/index.md) | [common]<br>class [ForgetPasswordUseCase](-forget-password-use-case/index.md)(authConfig: [AuthConfig](../multi.platform.auth.shared.external/-auth-config/index.md), userRepository: [UserRepository](../multi.platform.auth.shared.domain.user/-user-repository/index.md)) |
| [RegisterUseCase](-register-use-case/index.md) | [common]<br>class [RegisterUseCase](-register-use-case/index.md)(authConfig: [AuthConfig](../multi.platform.auth.shared.external/-auth-config/index.md), userRepository: [UserRepository](../multi.platform.auth.shared.domain.user/-user-repository/index.md)) |
| [SignInEmailUseCase](-sign-in-email-use-case/index.md) | [common]<br>class [SignInEmailUseCase](-sign-in-email-use-case/index.md)(authConfig: [AuthConfig](../multi.platform.auth.shared.external/-auth-config/index.md), userRepository: [UserRepository](../multi.platform.auth.shared.domain.user/-user-repository/index.md)) |
| [SignInProviderUseCase](-sign-in-provider-use-case/index.md) | [common]<br>class [SignInProviderUseCase](-sign-in-provider-use-case/index.md)(authConfig: [AuthConfig](../multi.platform.auth.shared.external/-auth-config/index.md), userRepository: [UserRepository](../multi.platform.auth.shared.domain.user/-user-repository/index.md)) |
| [SignOutUseCase](-sign-out-use-case/index.md) | [common]<br>class [SignOutUseCase](-sign-out-use-case/index.md)(authConfig: [AuthConfig](../multi.platform.auth.shared.external/-auth-config/index.md), userRepository: [UserRepository](../multi.platform.auth.shared.domain.user/-user-repository/index.md)) |
| [ValidatePhoneUseCase](-validate-phone-use-case/index.md) | [common]<br>class [ValidatePhoneUseCase](-validate-phone-use-case/index.md)(authConfig: [AuthConfig](../multi.platform.auth.shared.external/-auth-config/index.md), userRepository: [UserRepository](../multi.platform.auth.shared.domain.user/-user-repository/index.md)) |
| [VerifyOtpUseCase](-verify-otp-use-case/index.md) | [common]<br>class [VerifyOtpUseCase](-verify-otp-use-case/index.md)(authConfig: [AuthConfig](../multi.platform.auth.shared.external/-auth-config/index.md), userRepository: [UserRepository](../multi.platform.auth.shared.domain.user/-user-repository/index.md)) |

//[auth_shared](../../../index.md)/[multi.platform.auth.shared.domain.user.usecase](../index.md)/[VerifyOtpUseCase](index.md)

# VerifyOtpUseCase

[common]\
class [VerifyOtpUseCase](index.md)(authConfig: [AuthConfig](../../multi.platform.auth.shared.external/-auth-config/index.md), userRepository: [UserRepository](../../multi.platform.auth.shared.domain.user/-user-repository/index.md))

## Constructors

| | |
|---|---|
| [VerifyOtpUseCase](-verify-otp-use-case.md) | [common]<br>constructor(authConfig: [AuthConfig](../../multi.platform.auth.shared.external/-auth-config/index.md), userRepository: [UserRepository](../../multi.platform.auth.shared.domain.user/-user-repository/index.md)) |

## Functions

| Name | Summary |
|---|---|
| [invoke](invoke.md) | [common]<br>suspend operator fun [invoke](invoke.md)(otp: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html), type: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html), phone: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)): [Ticket](../../multi.platform.auth.shared.domain.user.entity/-ticket/index.md)? |

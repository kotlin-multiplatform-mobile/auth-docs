//[auth_shared](../../../index.md)/[multi.platform.auth.shared.domain.user.usecase](../index.md)/[SignOutUseCase](index.md)

# SignOutUseCase

[common]\
class [SignOutUseCase](index.md)(authConfig: [AuthConfig](../../multi.platform.auth.shared.external/-auth-config/index.md), userRepository: [UserRepository](../../multi.platform.auth.shared.domain.user/-user-repository/index.md))

## Constructors

| | |
|---|---|
| [SignOutUseCase](-sign-out-use-case.md) | [common]<br>constructor(authConfig: [AuthConfig](../../multi.platform.auth.shared.external/-auth-config/index.md), userRepository: [UserRepository](../../multi.platform.auth.shared.domain.user/-user-repository/index.md)) |

## Functions

| Name | Summary |
|---|---|
| [invoke](invoke.md) | [common]<br>suspend operator fun [invoke](invoke.md)(accessToken: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?): [Ticket](../../multi.platform.auth.shared.domain.user.entity/-ticket/index.md)? |

//[auth_shared](../../../index.md)/[multi.platform.auth.shared.domain.user.usecase](../index.md)/[SignOutUseCase](index.md)/[invoke](invoke.md)

# invoke

[common]\
suspend operator fun [invoke](invoke.md)(accessToken: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?): [Ticket](../../multi.platform.auth.shared.domain.user.entity/-ticket/index.md)?

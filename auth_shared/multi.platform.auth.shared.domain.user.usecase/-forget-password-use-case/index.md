//[auth_shared](../../../index.md)/[multi.platform.auth.shared.domain.user.usecase](../index.md)/[ForgetPasswordUseCase](index.md)

# ForgetPasswordUseCase

[common]\
class [ForgetPasswordUseCase](index.md)(authConfig: [AuthConfig](../../multi.platform.auth.shared.external/-auth-config/index.md), userRepository: [UserRepository](../../multi.platform.auth.shared.domain.user/-user-repository/index.md))

## Constructors

| | |
|---|---|
| [ForgetPasswordUseCase](-forget-password-use-case.md) | [common]<br>constructor(authConfig: [AuthConfig](../../multi.platform.auth.shared.external/-auth-config/index.md), userRepository: [UserRepository](../../multi.platform.auth.shared.domain.user/-user-repository/index.md)) |

## Functions

| Name | Summary |
|---|---|
| [invoke](invoke.md) | [common]<br>suspend operator fun [invoke](invoke.md)(email: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)): [Ticket](../../multi.platform.auth.shared.domain.user.entity/-ticket/index.md)? |

//[auth_shared](../../../index.md)/[multi.platform.auth.shared.domain.user.usecase](../index.md)/[AuthorizationUseCase](index.md)/[AuthorizationUseCase](-authorization-use-case.md)

# AuthorizationUseCase

[common]\
constructor(authConfig: [AuthConfig](../../multi.platform.auth.shared.external/-auth-config/index.md), userRepository: [UserRepository](../../multi.platform.auth.shared.domain.user/-user-repository/index.md))

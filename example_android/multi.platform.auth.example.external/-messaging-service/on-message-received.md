//[example_android](../../../index.md)/[multi.platform.auth.example.external](../index.md)/[MessagingService](index.md)/[onMessageReceived](on-message-received.md)

# onMessageReceived

[androidJvm]\
open override fun [onMessageReceived](on-message-received.md)(remoteMessage: RemoteMessage)

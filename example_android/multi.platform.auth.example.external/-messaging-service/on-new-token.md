//[example_android](../../../index.md)/[multi.platform.auth.example.external](../index.md)/[MessagingService](index.md)/[onNewToken](on-new-token.md)

# onNewToken

[androidJvm]\
open override fun [onNewToken](on-new-token.md)(token: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html))

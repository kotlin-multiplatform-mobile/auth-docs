//[example_android](../../../index.md)/[multi.platform.auth.example.external](../index.md)/[Worker](index.md)/[Worker](-worker.md)

# Worker

[androidJvm]\
constructor(appContext: [Context](https://developer.android.com/reference/kotlin/android/content/Context.html), workerParams: [WorkerParameters](https://developer.android.com/reference/kotlin/androidx/work/WorkerParameters.html))

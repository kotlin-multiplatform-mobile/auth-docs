//[example_android](../../../index.md)/[multi.platform.auth.example.external](../index.md)/[Worker](index.md)/[doWork](do-work.md)

# doWork

[androidJvm]\
open override fun [doWork](do-work.md)(): [ListenableWorker.Result](https://developer.android.com/reference/kotlin/androidx/work/ListenableWorker.Result.html)

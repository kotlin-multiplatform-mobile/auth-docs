//[example_android](../../../index.md)/[multi.platform.auth.example.external](../index.md)/[AuthConfigImpl](index.md)/[countryFlag](country-flag.md)

# countryFlag

[androidJvm]\
open override val [countryFlag](country-flag.md): [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html)

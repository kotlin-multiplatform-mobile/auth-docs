//[example_android](../../../index.md)/[multi.platform.auth.example.external](../index.md)/[AuthConfigImpl](index.md)

# AuthConfigImpl

[androidJvm]\
class [AuthConfigImpl](index.md)(val rootView: [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html) = R.id.cl_root, val logo: [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html) = R.mipmap.ic_launcher, val countryFlag: [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html) = R.drawable.country, val headerTransactionIdKey: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) = &quot;x-header-transaction-id&quot;, val host: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) = BuildConfig.AUTH_SERVER, val signInByEmailApi: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) = &quot;/login&quot;, val signInByProviderApi: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) = &quot;/login/google&quot;, val signOutApi: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) = &quot;&quot;, val registerApi: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) = &quot;/register&quot;, val forgetPasswordApi: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) = &quot;/forget&quot;, val signInByPhoneApi: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) = &quot;&quot;, val validatePhoneApi: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) = &quot;&quot;, val verifyOtpApi: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) = &quot;&quot;) : AuthConfig

## Constructors

| | |
|---|---|
| [AuthConfigImpl](-auth-config-impl.md) | [androidJvm]<br>constructor(rootView: [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html) = R.id.cl_root, logo: [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html) = R.mipmap.ic_launcher, countryFlag: [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html) = R.drawable.country, headerTransactionIdKey: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) = &quot;x-header-transaction-id&quot;, host: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) = BuildConfig.AUTH_SERVER, signInByEmailApi: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) = &quot;/login&quot;, signInByProviderApi: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) = &quot;/login/google&quot;, signOutApi: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) = &quot;&quot;, registerApi: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) = &quot;/register&quot;, forgetPasswordApi: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) = &quot;/forget&quot;, signInByPhoneApi: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) = &quot;&quot;, validatePhoneApi: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) = &quot;&quot;, verifyOtpApi: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) = &quot;&quot;) |

## Properties

| Name | Summary |
|---|---|
| [countryFlag](country-flag.md) | [androidJvm]<br>open override val [countryFlag](country-flag.md): [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html) |
| [forgetPasswordApi](forget-password-api.md) | [androidJvm]<br>open override val [forgetPasswordApi](forget-password-api.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) |
| [headerTransactionIdKey](header-transaction-id-key.md) | [androidJvm]<br>open override val [headerTransactionIdKey](header-transaction-id-key.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) |
| [host](host.md) | [androidJvm]<br>open override val [host](host.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) |
| [logo](logo.md) | [androidJvm]<br>open override val [logo](logo.md): [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html) |
| [registerApi](register-api.md) | [androidJvm]<br>open override val [registerApi](register-api.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) |
| [rootView](root-view.md) | [androidJvm]<br>open override val [rootView](root-view.md): [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html) |
| [signInByEmailApi](sign-in-by-email-api.md) | [androidJvm]<br>open override val [signInByEmailApi](sign-in-by-email-api.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) |
| [signInByPhoneApi](sign-in-by-phone-api.md) | [androidJvm]<br>open override val [signInByPhoneApi](sign-in-by-phone-api.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) |
| [signInByProviderApi](sign-in-by-provider-api.md) | [androidJvm]<br>open override val [signInByProviderApi](sign-in-by-provider-api.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) |
| [signOutApi](sign-out-api.md) | [androidJvm]<br>open override val [signOutApi](sign-out-api.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) |
| [validatePhoneApi](validate-phone-api.md) | [androidJvm]<br>open override val [validatePhoneApi](validate-phone-api.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) |
| [verifyOtpApi](verify-otp-api.md) | [androidJvm]<br>open override val [verifyOtpApi](verify-otp-api.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) |

## Functions

| Name | Summary |
|---|---|
| [forgetPasswordMapper](forget-password-mapper.md) | [androidJvm]<br>open override fun [forgetPasswordMapper](forget-password-mapper.md)(jsonObject: JsonObject?): Ticket? |
| [registerMapper](register-mapper.md) | [androidJvm]<br>open override fun [registerMapper](register-mapper.md)(jsonObject: JsonObject?): Ticket? |
| [signInMapper](sign-in-mapper.md) | [androidJvm]<br>open override fun [signInMapper](sign-in-mapper.md)(jsonObject: JsonObject?): Ticket? |
| [signOutMapper](sign-out-mapper.md) | [androidJvm]<br>open override fun [signOutMapper](sign-out-mapper.md)(jsonObject: JsonObject?): Ticket? |
| [validatePhoneMapper](validate-phone-mapper.md) | [androidJvm]<br>open override fun [validatePhoneMapper](validate-phone-mapper.md)(jsonObject: JsonObject?): Ticket? |
| [verifyOtpMapper](verify-otp-mapper.md) | [androidJvm]<br>open override fun [verifyOtpMapper](verify-otp-mapper.md)(jsonObject: JsonObject?): Ticket? |

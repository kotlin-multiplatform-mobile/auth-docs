//[example_android](../../../index.md)/[multi.platform.auth.example.external](../index.md)/[AuthConfigImpl](index.md)/[signInByProviderApi](sign-in-by-provider-api.md)

# signInByProviderApi

[androidJvm]\
open override val [signInByProviderApi](sign-in-by-provider-api.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)

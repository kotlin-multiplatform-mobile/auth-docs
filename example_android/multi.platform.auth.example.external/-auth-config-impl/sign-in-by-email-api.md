//[example_android](../../../index.md)/[multi.platform.auth.example.external](../index.md)/[AuthConfigImpl](index.md)/[signInByEmailApi](sign-in-by-email-api.md)

# signInByEmailApi

[androidJvm]\
open override val [signInByEmailApi](sign-in-by-email-api.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)

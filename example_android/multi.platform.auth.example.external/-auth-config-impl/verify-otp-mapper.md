//[example_android](../../../index.md)/[multi.platform.auth.example.external](../index.md)/[AuthConfigImpl](index.md)/[verifyOtpMapper](verify-otp-mapper.md)

# verifyOtpMapper

[androidJvm]\
open override fun [verifyOtpMapper](verify-otp-mapper.md)(jsonObject: JsonObject?): Ticket?

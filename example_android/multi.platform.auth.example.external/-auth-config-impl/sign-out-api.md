//[example_android](../../../index.md)/[multi.platform.auth.example.external](../index.md)/[AuthConfigImpl](index.md)/[signOutApi](sign-out-api.md)

# signOutApi

[androidJvm]\
open override val [signOutApi](sign-out-api.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)

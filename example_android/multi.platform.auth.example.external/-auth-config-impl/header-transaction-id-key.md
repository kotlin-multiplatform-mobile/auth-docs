//[example_android](../../../index.md)/[multi.platform.auth.example.external](../index.md)/[AuthConfigImpl](index.md)/[headerTransactionIdKey](header-transaction-id-key.md)

# headerTransactionIdKey

[androidJvm]\
open override val [headerTransactionIdKey](header-transaction-id-key.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)

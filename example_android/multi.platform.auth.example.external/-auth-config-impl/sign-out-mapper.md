//[example_android](../../../index.md)/[multi.platform.auth.example.external](../index.md)/[AuthConfigImpl](index.md)/[signOutMapper](sign-out-mapper.md)

# signOutMapper

[androidJvm]\
open override fun [signOutMapper](sign-out-mapper.md)(jsonObject: JsonObject?): Ticket?

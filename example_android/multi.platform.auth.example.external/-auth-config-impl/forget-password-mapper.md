//[example_android](../../../index.md)/[multi.platform.auth.example.external](../index.md)/[AuthConfigImpl](index.md)/[forgetPasswordMapper](forget-password-mapper.md)

# forgetPasswordMapper

[androidJvm]\
open override fun [forgetPasswordMapper](forget-password-mapper.md)(jsonObject: JsonObject?): Ticket?

//[example_android](../../../index.md)/[multi.platform.auth.example.external](../index.md)/[CoreConfigImpl](index.md)

# CoreConfigImpl

[androidJvm]\
class [CoreConfigImpl](index.md)(val apiAuthPath: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? = &quot;/login&quot;, val apiChannel: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? = &quot;mobile&quot;, val apiRefreshTokenPath: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? = &quot;/refresh&quot;, val headerChannel: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? = &quot;x-header-channel&quot;, val headerDeviceId: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? = &quot;x-header-device-id&quot;, val headerLanguage: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? = &quot;x-header-language&quot;, val headerOs: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? = &quot;x-header-version&quot;, val headerVersion: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? = &quot;x-header-version&quot;) : CoreConfig

## Constructors

| | |
|---|---|
| [CoreConfigImpl](-core-config-impl.md) | [androidJvm]<br>constructor(apiAuthPath: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? = &quot;/login&quot;, apiChannel: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? = &quot;mobile&quot;, apiRefreshTokenPath: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? = &quot;/refresh&quot;, headerChannel: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? = &quot;x-header-channel&quot;, headerDeviceId: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? = &quot;x-header-device-id&quot;, headerLanguage: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? = &quot;x-header-language&quot;, headerOs: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? = &quot;x-header-version&quot;, headerVersion: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? = &quot;x-header-version&quot;) |

## Properties

| Name | Summary |
|---|---|
| [apiAuthPath](api-auth-path.md) | [androidJvm]<br>open override val [apiAuthPath](api-auth-path.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? |
| [apiChannel](api-channel.md) | [androidJvm]<br>open override val [apiChannel](api-channel.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? |
| [apiRefreshTokenPath](api-refresh-token-path.md) | [androidJvm]<br>open override val [apiRefreshTokenPath](api-refresh-token-path.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? |
| [headerChannel](header-channel.md) | [androidJvm]<br>open override val [headerChannel](header-channel.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? |
| [headerDeviceId](header-device-id.md) | [androidJvm]<br>open override val [headerDeviceId](header-device-id.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? |
| [headerLanguage](header-language.md) | [androidJvm]<br>open override val [headerLanguage](header-language.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? |
| [headerOs](header-os.md) | [androidJvm]<br>open override val [headerOs](header-os.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? |
| [headerVersion](header-version.md) | [androidJvm]<br>open override val [headerVersion](header-version.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? |

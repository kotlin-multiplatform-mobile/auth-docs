//[example_android](../../../index.md)/[multi.platform.auth.example.external](../index.md)/[CoreConfigImpl](index.md)/[apiRefreshTokenPath](api-refresh-token-path.md)

# apiRefreshTokenPath

[androidJvm]\
open override val [apiRefreshTokenPath](api-refresh-token-path.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?

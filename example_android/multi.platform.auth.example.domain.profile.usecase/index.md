//[example_android](../../index.md)/[multi.platform.auth.example.domain.profile.usecase](index.md)

# Package-level declarations

## Types

| Name | Summary |
|---|---|
| [GetProfileUseCase](-get-profile-use-case/index.md) | [androidJvm]<br>class [GetProfileUseCase](-get-profile-use-case/index.md)(profileRepository: [ProfileRepository](../multi.platform.auth.example.domain.profile/-profile-repository/index.md)) |

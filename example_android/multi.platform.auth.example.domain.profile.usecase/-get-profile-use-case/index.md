//[example_android](../../../index.md)/[multi.platform.auth.example.domain.profile.usecase](../index.md)/[GetProfileUseCase](index.md)

# GetProfileUseCase

[androidJvm]\
class [GetProfileUseCase](index.md)(profileRepository: [ProfileRepository](../../multi.platform.auth.example.domain.profile/-profile-repository/index.md))

## Constructors

| | |
|---|---|
| [GetProfileUseCase](-get-profile-use-case.md) | [androidJvm]<br>constructor(profileRepository: [ProfileRepository](../../multi.platform.auth.example.domain.profile/-profile-repository/index.md)) |

## Functions

| Name | Summary |
|---|---|
| [invoke](invoke.md) | [androidJvm]<br>suspend operator fun [invoke](invoke.md)(accessToken: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?, versionName: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?, androidId: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?, playerId: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?): [GetProfileResp](../../multi.platform.auth.example.data.profile.network/-get-profile-resp/index.md)? |

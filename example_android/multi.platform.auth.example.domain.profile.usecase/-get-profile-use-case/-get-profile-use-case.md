//[example_android](../../../index.md)/[multi.platform.auth.example.domain.profile.usecase](../index.md)/[GetProfileUseCase](index.md)/[GetProfileUseCase](-get-profile-use-case.md)

# GetProfileUseCase

[androidJvm]\
constructor(profileRepository: [ProfileRepository](../../multi.platform.auth.example.domain.profile/-profile-repository/index.md))

//[example_android](../../index.md)/[multi.platform.auth.example](index.md)

# Package-level declarations

## Types

| Name | Summary |
|---|---|
| [ExampleApplication](-example-application/index.md) | [androidJvm]<br>class [ExampleApplication](-example-application/index.md) : CoreApplication |

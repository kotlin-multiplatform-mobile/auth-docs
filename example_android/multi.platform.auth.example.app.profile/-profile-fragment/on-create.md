//[example_android](../../../index.md)/[multi.platform.auth.example.app.profile](../index.md)/[ProfileFragment](index.md)/[onCreate](on-create.md)

# onCreate

[androidJvm]\
open override fun [onCreate](on-create.md)(savedInstanceState: [Bundle](https://developer.android.com/reference/kotlin/android/os/Bundle.html)?)

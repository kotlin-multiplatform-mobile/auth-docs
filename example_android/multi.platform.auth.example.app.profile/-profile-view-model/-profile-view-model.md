//[example_android](../../../index.md)/[multi.platform.auth.example.app.profile](../index.md)/[ProfileViewModel](index.md)/[ProfileViewModel](-profile-view-model.md)

# ProfileViewModel

[androidJvm]\
constructor(getProfileUseCase: [GetProfileUseCase](../../multi.platform.auth.example.domain.profile.usecase/-get-profile-use-case/index.md))

//[example_android](../../../index.md)/[multi.platform.auth.example.app.profile](../index.md)/[ProfileViewModel](index.md)/[getProfile](get-profile.md)

# getProfile

[androidJvm]\
fun [getProfile](get-profile.md)(versionName: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?, androidId: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?, playerId: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?)

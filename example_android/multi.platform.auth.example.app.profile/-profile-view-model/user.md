//[example_android](../../../index.md)/[multi.platform.auth.example.app.profile](../index.md)/[ProfileViewModel](index.md)/[user](user.md)

# user

[androidJvm]\
val [user](user.md): StateFlow&lt;[User](../../multi.platform.auth.example.domain.profile.entity/-user/index.md)?&gt;

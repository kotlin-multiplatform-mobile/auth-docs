//[example_android](../../../index.md)/[multi.platform.auth.example.app.profile](../index.md)/[ProfileViewModel](index.md)

# ProfileViewModel

[androidJvm]\
class [ProfileViewModel](index.md)(getProfileUseCase: [GetProfileUseCase](../../multi.platform.auth.example.domain.profile.usecase/-get-profile-use-case/index.md)) : CoreViewModel

## Constructors

| | |
|---|---|
| [ProfileViewModel](-profile-view-model.md) | [androidJvm]<br>constructor(getProfileUseCase: [GetProfileUseCase](../../multi.platform.auth.example.domain.profile.usecase/-get-profile-use-case/index.md)) |

## Properties

| Name | Summary |
|---|---|
| [accessToken](index.md#-74023671%2FProperties%2F1184549950) | [androidJvm]<br>var [accessToken](index.md#-74023671%2FProperties%2F1184549950): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? |
| [errorConfirm](index.md#1488355492%2FProperties%2F1184549950) | [androidJvm]<br>var [errorConfirm](index.md#1488355492%2FProperties%2F1184549950): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? |
| [errorEmailFormat](index.md#-620529519%2FProperties%2F1184549950) | [androidJvm]<br>var [errorEmailFormat](index.md#-620529519%2FProperties%2F1184549950): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? |
| [errorEmptyField](index.md#832052137%2FProperties%2F1184549950) | [androidJvm]<br>var [errorEmptyField](index.md#832052137%2FProperties%2F1184549950): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? |
| [errorMessage](index.md#-366963395%2FProperties%2F1184549950) | [androidJvm]<br>val [errorMessage](index.md#-366963395%2FProperties%2F1184549950): MutableStateFlow&lt;[String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?&gt; |
| [errorMinChar](index.md#-784102500%2FProperties%2F1184549950) | [androidJvm]<br>var [errorMinChar](index.md#-784102500%2FProperties%2F1184549950): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? |
| [errorPasswordFormat](index.md#265462788%2FProperties%2F1184549950) | [androidJvm]<br>var [errorPasswordFormat](index.md#265462788%2FProperties%2F1184549950): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? |
| [errorPhoneFormat](index.md#-350172033%2FProperties%2F1184549950) | [androidJvm]<br>var [errorPhoneFormat](index.md#-350172033%2FProperties%2F1184549950): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? |
| [forceSignout](index.md#2001686774%2FProperties%2F1184549950) | [androidJvm]<br>val [forceSignout](index.md#2001686774%2FProperties%2F1184549950): MutableStateFlow&lt;[Boolean](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html)&gt; |
| [isEmpty](index.md#2032650747%2FProperties%2F1184549950) | [androidJvm]<br>val [isEmpty](index.md#2032650747%2FProperties%2F1184549950): MutableStateFlow&lt;[Boolean](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html)&gt; |
| [isFromNetwork](index.md#450452420%2FProperties%2F1184549950) | [androidJvm]<br>var [isFromNetwork](index.md#450452420%2FProperties%2F1184549950): [Boolean](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html) |
| [loadingIndicator](index.md#-315772695%2FProperties%2F1184549950) | [androidJvm]<br>val [loadingIndicator](index.md#-315772695%2FProperties%2F1184549950): MutableStateFlow&lt;[Boolean](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html)?&gt; |
| [onException](index.md#-2065585362%2FProperties%2F1184549950) | [androidJvm]<br>val [onException](index.md#-2065585362%2FProperties%2F1184549950): MutableStateFlow&lt;[Exception](https://developer.android.com/reference/kotlin/java/lang/Exception.html)?&gt; |
| [scope](index.md#1071716170%2FProperties%2F1184549950) | [androidJvm]<br>val [scope](index.md#1071716170%2FProperties%2F1184549950): CoroutineScope |
| [successMessage](index.md#-428149896%2FProperties%2F1184549950) | [androidJvm]<br>val [successMessage](index.md#-428149896%2FProperties%2F1184549950): MutableStateFlow&lt;[String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?&gt; |
| [toastMessage](index.md#-805167780%2FProperties%2F1184549950) | [androidJvm]<br>val [toastMessage](index.md#-805167780%2FProperties%2F1184549950): MutableStateFlow&lt;[String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?&gt; |
| [useAsyncNetworkCall](index.md#1408937991%2FProperties%2F1184549950) | [androidJvm]<br>var [useAsyncNetworkCall](index.md#1408937991%2FProperties%2F1184549950): [Boolean](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html) |
| [user](user.md) | [androidJvm]<br>val [user](user.md): StateFlow&lt;[User](../../multi.platform.auth.example.domain.profile.entity/-user/index.md)?&gt; |

## Functions

| Name | Summary |
|---|---|
| [addCloseable](index.md#264516373%2FFunctions%2F1184549950) | [androidJvm]<br>open fun [addCloseable](index.md#264516373%2FFunctions%2F1184549950)(@[NonNull](https://developer.android.com/reference/kotlin/androidx/annotation/NonNull.html)p0: [Closeable](https://developer.android.com/reference/kotlin/java/io/Closeable.html)) |
| [clear](clear.md) | [androidJvm]<br>fun [clear](clear.md)() |
| [getProfile](get-profile.md) | [androidJvm]<br>fun [getProfile](get-profile.md)(versionName: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?, androidId: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?, playerId: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?) |
| [validateBlank](index.md#-597343809%2FFunctions%2F1184549950) | [androidJvm]<br>fun [validateBlank](index.md#-597343809%2FFunctions%2F1184549950)(field: MutableStateFlow&lt;[String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?&gt;, error: MutableStateFlow&lt;[String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?&gt;): [Boolean](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html) |
| [validateConfirm](index.md#-944246634%2FFunctions%2F1184549950) | [androidJvm]<br>fun [validateConfirm](index.md#-944246634%2FFunctions%2F1184549950)(field1: MutableStateFlow&lt;[String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?&gt;, field2: MutableStateFlow&lt;[String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?&gt;, error: MutableStateFlow&lt;[String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?&gt;): [Boolean](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html) |
| [validateEmailFormat](index.md#52700574%2FFunctions%2F1184549950) | [androidJvm]<br>fun [validateEmailFormat](index.md#52700574%2FFunctions%2F1184549950)(field: MutableStateFlow&lt;[String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?&gt;, error: MutableStateFlow&lt;[String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?&gt;): [Boolean](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html) |
| [validateMinChar](index.md#2003153546%2FFunctions%2F1184549950) | [androidJvm]<br>fun [validateMinChar](index.md#2003153546%2FFunctions%2F1184549950)(min: [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html), field: MutableStateFlow&lt;[String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?&gt;, error: MutableStateFlow&lt;[String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?&gt;): [Boolean](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html) |
| [validatePasswordFormat](index.md#1543656427%2FFunctions%2F1184549950) | [androidJvm]<br>fun [validatePasswordFormat](index.md#1543656427%2FFunctions%2F1184549950)(field: MutableStateFlow&lt;[String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?&gt;, error: MutableStateFlow&lt;[String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?&gt;): [Boolean](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html) |
| [validatePhoneFormat](index.md#2112099184%2FFunctions%2F1184549950) | [androidJvm]<br>fun [validatePhoneFormat](index.md#2112099184%2FFunctions%2F1184549950)(field: MutableStateFlow&lt;[String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?&gt;, error: MutableStateFlow&lt;[String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?&gt;): [Boolean](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html) |

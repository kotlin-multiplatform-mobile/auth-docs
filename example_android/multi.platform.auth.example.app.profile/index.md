//[example_android](../../index.md)/[multi.platform.auth.example.app.profile](index.md)

# Package-level declarations

## Types

| Name | Summary |
|---|---|
| [ProfileFragment](-profile-fragment/index.md) | [androidJvm]<br>class [ProfileFragment](-profile-fragment/index.md) : CoreFragment |
| [ProfileViewModel](-profile-view-model/index.md) | [androidJvm]<br>class [ProfileViewModel](-profile-view-model/index.md)(getProfileUseCase: [GetProfileUseCase](../multi.platform.auth.example.domain.profile.usecase/-get-profile-use-case/index.md)) : CoreViewModel |

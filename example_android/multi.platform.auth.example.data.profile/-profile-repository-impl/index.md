//[example_android](../../../index.md)/[multi.platform.auth.example.data.profile](../index.md)/[ProfileRepositoryImpl](index.md)

# ProfileRepositoryImpl

[androidJvm]\
class [ProfileRepositoryImpl](index.md)(apiClientProvider: ApiClientProvider&lt;HttpClient&gt;) : [ProfileRepository](../../multi.platform.auth.example.domain.profile/-profile-repository/index.md)

## Constructors

| | |
|---|---|
| [ProfileRepositoryImpl](-profile-repository-impl.md) | [androidJvm]<br>constructor(apiClientProvider: ApiClientProvider&lt;HttpClient&gt;) |

## Functions

| Name | Summary |
|---|---|
| [getProfile](get-profile.md) | [androidJvm]<br>open suspend override fun [getProfile](get-profile.md)(accessToken: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?, versionName: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?, androidId: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?, playerId: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?): [GetProfileResp](../../multi.platform.auth.example.data.profile.network/-get-profile-resp/index.md)? |

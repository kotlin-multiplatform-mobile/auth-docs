//[example_android](../../../index.md)/[multi.platform.auth.example.data.profile](../index.md)/[ProfileRepositoryImpl](index.md)/[getProfile](get-profile.md)

# getProfile

[androidJvm]\
open suspend override fun [getProfile](get-profile.md)(accessToken: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?, versionName: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?, androidId: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?, playerId: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?): [GetProfileResp](../../multi.platform.auth.example.data.profile.network/-get-profile-resp/index.md)?

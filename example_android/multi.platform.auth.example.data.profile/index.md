//[example_android](../../index.md)/[multi.platform.auth.example.data.profile](index.md)

# Package-level declarations

## Types

| Name | Summary |
|---|---|
| [ProfileRepositoryImpl](-profile-repository-impl/index.md) | [androidJvm]<br>class [ProfileRepositoryImpl](-profile-repository-impl/index.md)(apiClientProvider: ApiClientProvider&lt;HttpClient&gt;) : [ProfileRepository](../multi.platform.auth.example.domain.profile/-profile-repository/index.md) |

//[example_android](../../../index.md)/[multi.platform.auth.example.data.profile.network](../index.md)/[GetProfileResp](index.md)/[success](success.md)

# success

[androidJvm]\
val [success](success.md): [Boolean](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html)

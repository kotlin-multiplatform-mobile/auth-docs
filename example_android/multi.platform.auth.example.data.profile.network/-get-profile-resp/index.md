//[example_android](../../../index.md)/[multi.platform.auth.example.data.profile.network](../index.md)/[GetProfileResp](index.md)

# GetProfileResp

[androidJvm]\
@Serializable

data class [GetProfileResp](index.md)(val success: [Boolean](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html), val message: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html), val user: [User](../../multi.platform.auth.example.domain.profile.entity/-user/index.md))

## Constructors

| | |
|---|---|
| [GetProfileResp](-get-profile-resp.md) | [androidJvm]<br>constructor(success: [Boolean](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html), message: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html), user: [User](../../multi.platform.auth.example.domain.profile.entity/-user/index.md)) |

## Properties

| Name | Summary |
|---|---|
| [message](message.md) | [androidJvm]<br>val [message](message.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) |
| [success](success.md) | [androidJvm]<br>val [success](success.md): [Boolean](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html) |
| [user](user.md) | [androidJvm]<br>val [user](user.md): [User](../../multi.platform.auth.example.domain.profile.entity/-user/index.md) |

//[example_android](../../../index.md)/[multi.platform.auth.example.data.profile.network](../index.md)/[GetProfileResp](index.md)/[user](user.md)

# user

[androidJvm]\
val [user](user.md): [User](../../multi.platform.auth.example.domain.profile.entity/-user/index.md)

//[example_android](../../../index.md)/[multi.platform.auth.example.data.profile.network](../index.md)/[GetProfileResp](index.md)/[GetProfileResp](-get-profile-resp.md)

# GetProfileResp

[androidJvm]\
constructor(success: [Boolean](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html), message: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html), user: [User](../../multi.platform.auth.example.domain.profile.entity/-user/index.md))

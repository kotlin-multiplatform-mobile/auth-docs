//[example_android](../../../index.md)/[multi.platform.auth.example.data.profile.network](../index.md)/[GetProfileResp](index.md)/[message](message.md)

# message

[androidJvm]\
val [message](message.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)

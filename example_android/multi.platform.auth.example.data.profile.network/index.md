//[example_android](../../index.md)/[multi.platform.auth.example.data.profile.network](index.md)

# Package-level declarations

## Types

| Name | Summary |
|---|---|
| [GetProfileResp](-get-profile-resp/index.md) | [androidJvm]<br>@Serializable<br>data class [GetProfileResp](-get-profile-resp/index.md)(val success: [Boolean](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html), val message: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html), val user: [User](../multi.platform.auth.example.domain.profile.entity/-user/index.md)) |

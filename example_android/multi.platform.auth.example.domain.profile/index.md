//[example_android](../../index.md)/[multi.platform.auth.example.domain.profile](index.md)

# Package-level declarations

## Types

| Name | Summary |
|---|---|
| [ProfileRepository](-profile-repository/index.md) | [androidJvm]<br>interface [ProfileRepository](-profile-repository/index.md) |

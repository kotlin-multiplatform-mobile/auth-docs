//[example_android](../../../index.md)/[multi.platform.auth.example.domain.profile](../index.md)/[ProfileRepository](index.md)/[getProfile](get-profile.md)

# getProfile

[androidJvm]\
abstract suspend fun [getProfile](get-profile.md)(accessToken: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?, versionName: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?, androidId: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?, playerId: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?): [GetProfileResp](../../multi.platform.auth.example.data.profile.network/-get-profile-resp/index.md)?

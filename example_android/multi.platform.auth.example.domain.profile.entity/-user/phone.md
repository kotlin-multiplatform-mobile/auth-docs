//[example_android](../../../index.md)/[multi.platform.auth.example.domain.profile.entity](../index.md)/[User](index.md)/[phone](phone.md)

# phone

[androidJvm]\
var [phone](phone.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?

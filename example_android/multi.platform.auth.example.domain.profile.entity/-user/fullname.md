//[example_android](../../../index.md)/[multi.platform.auth.example.domain.profile.entity](../index.md)/[User](index.md)/[fullname](fullname.md)

# fullname

[androidJvm]\
var [fullname](fullname.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?

//[example_android](../../../index.md)/[multi.platform.auth.example.domain.profile.entity](../index.md)/[User](index.md)/[email](email.md)

# email

[androidJvm]\
var [email](email.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?

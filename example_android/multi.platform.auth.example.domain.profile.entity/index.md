//[example_android](../../index.md)/[multi.platform.auth.example.domain.profile.entity](index.md)

# Package-level declarations

## Types

| Name | Summary |
|---|---|
| [User](-user/index.md) | [androidJvm]<br>@Serializable<br>data class [User](-user/index.md)(var id: [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html) = 0, var fullname: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? = null, var email: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? = null, var phone: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? = null) |
